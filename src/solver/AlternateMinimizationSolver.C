
#include "AlternateMinimizationSolver.h"
#include "SNESFunction.h"
#include <petsc/private/snesimpl.h>
#include <petscsnes.h>

/* Our own solver, to be registered at runtime-solver */
#undef __FUNCT__
#define __FUNCT__ "SNESReset_AlternateMinimization_Solver"
PetscErrorCode SNESReset_AlternateMinimization_Solver(SNES /*snes*/) {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
/*
 SNESDestroy_AlternateMinimization_Solver - Destroys the private
 SNES_AlternateMinimization_Solver" context that was created with
 SNESCreate_AlternateMinimization_Solver"().

 Input Parameter:
 . snes - the SNES context

 Application Interface Routine: SNESDestroy()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESDestroy_AlternateMinimization_Solver"
PetscErrorCode SNESDestroy_AlternateMinimization_Solver(SNES snes) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = SNESReset_AlternateMinimization_Solver(snes);
  CHKERRQ(ierr);

  // SNES_PASSO * passo;
  // passo = (SNES_PASSO *)snes->data;
  // passo->interpolations.clear();
  // delete passo;
  PetscFunctionReturn(0);
}
/*
 SNESSetUp_AlternateMinimization_Solver - Sets up the internal data structures
 for the later use of the SNESSetUp_AlternateMinimization_Solver nonlinear
 solver.

 Input Parameters:
 +  snes - the SNES context
 -  x - the solution vector

 Application Interface Routine: SNESSetUp()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESSetUp_AlternateMinimization_Solver"
PetscErrorCode SNESSetUp_AlternateMinimization_Solver(SNES /*snes*/) {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
/*
 SNESSetFromOptions__AlternateMinimization_Solver - Sets various parameters for
 the SNESAlternateMinimizationLS method.

 Input Parameter:
 . snes - the SNES context

 Application Interface Routine: SNESSetFromOptions()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESSetFromOptions_AlternateMinimization_Solver"
static PetscErrorCode SNESSetFromOptions_AlternateMinimization_Solver(
    PetscOptionItems *PetscOptionsObject, SNES snes) {
  PetscFunctionBegin;

  SNES_AlternateMinimization *ctx = (SNES_AlternateMinimization *)snes->data;

  PetscOptionsBool("-snes_am_exact_solve", "exact_solve", "", ctx->exact_solve,
                   &ctx->exact_solve, NULL);

  PetscOptionsBool("-snes_am_direct_solver", "direct_solver", "",
                   ctx->direct_solver, &ctx->direct_solver, NULL);

  PetscOptionsScalar("-snes_am_disp_diff_tol", "snes_am_disp_diff_tol", "",
                     ctx->snes_am_disp_diff_tol, &ctx->snes_am_disp_diff_tol,
                     NULL);

  PetscOptionsScalar("-snes_am_c_diff_tol", "snes_am_c_diff_tol", "",
                     ctx->snes_am_c_diff_tol, &ctx->snes_am_c_diff_tol, NULL);

  PetscFunctionReturn(0);
}
/*
 SNESView_AlternateMinimization_Solver - Prints info from the
 SNES_AlternateMinimization_Solver data structure.

 Input Parameters:
 + SNES - the SNES context
 - viewer - visualization context

 Application Interface Routine: SNESView()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESView_AlternateMinimization_Solver"
static PetscErrorCode
SNESView_AlternateMinimization_Solver(SNES /*snes*/, PetscViewer viewer) {
  PetscBool iascii;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer, PETSCVIEWERASCII, &iascii);
  CHKERRQ(ierr);
  if (iascii) {
  }
  PetscFunctionReturn(0);
}
/*
 SNESSolve_AlternateMinimization_Solver - Solves a nonlinear system with the
 AlternateMinimization_Solver method.

 Input Parameters:
 . snes - the SNES context

 Output Parameter:
 . outits - number of iterations until termination

 Application Interface Routine: SNESSolve()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESSolve_AlternateMinimization_Solver"
PetscErrorCode SNESSolve_AlternateMinimization_Solver(SNES snes) {

  PetscFunctionBegin;
  SNES_AlternateMinimization *neP = (SNES_AlternateMinimization *)snes->data;

  {
    using namespace utopia;

    Vec X_global = snes->vec_sol;
    Vec X_disp = neP->sub_sness[0]->vec_sol;
    Vec X_pf = neP->sub_sness[1]->vec_sol;

    utopia::PetscVector uX_global, uX_disp, uX_pf;
    convert(X_global, uX_global);
    convert(X_disp, uX_disp);
    convert(X_pf, uX_pf);

    PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector>
        fun_global(snes, uX_global, uX_global);

    PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector> fun_disp(
        neP->sub_sness[0], uX_disp, uX_disp);

    auto fun_disp_ptr = std::make_shared<
        PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector>>(
        fun_disp);

    PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector> fun_pf(
        neP->sub_sness[1], uX_pf, uX_pf);

    auto fun_pf_ptr = std::make_shared<
        PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector>>(
        fun_pf);

    std::shared_ptr<LinearSolver<utopia::PetscMatrix, utopia::PetscVector>>
        lsolver_disp;

    if (neP->direct_solver == PETSC_FALSE) {
      auto bicgstab = std::make_shared<BiCGStab<PetscMatrix, PetscVector>>();
      bicgstab->atol(1e-12);
      bicgstab->rtol(1e-12);
      bicgstab->stol(1e-12);
      bicgstab->max_it(100000);
      bicgstab->pc_type("hypre");
      bicgstab->verbose(false);

      lsolver_disp = bicgstab;
    } else {

      // lsolver_disp =
      //     std::make_shared<LUDecomposition<PetscMatrix, PetscVector>>();

      auto bicgstab = std::make_shared<BiCGStab<PetscMatrix, PetscVector>>();
      bicgstab->atol(1e-12);
      bicgstab->rtol(1e-12);
      bicgstab->stol(1e-12);
      bicgstab->max_it(100000);
      bicgstab->pc_type("lu");
      bicgstab->verbose(false);

      lsolver_disp = bicgstab;
    }

    auto nlsolver_disp =
        std::make_shared<Newton<utopia::PetscMatrix, utopia::PetscVector>>(
            lsolver_disp);
    nlsolver_disp->atol(1e-7);
    nlsolver_disp->rtol(1e-6);
    nlsolver_disp->stol(1e-8);
    if (neP->exact_solve == PETSC_FALSE) {
      nlsolver_disp->forcing_strategy(InexactNewtonForcingStartegies::CAI);
    }

    // auto ls_strategy_disp =
    //     std::make_shared<utopia::Backtracking<utopia::PetscVector>>();
    // nlsolver_disp->set_line_search_strategy(ls_strategy_disp);

    std::shared_ptr<LinearSolver<utopia::PetscMatrix, utopia::PetscVector>>
        lsolver_pf;

    if (neP->direct_solver == PETSC_FALSE) {
      auto bicgstab = std::make_shared<BiCGStab<PetscMatrix, PetscVector>>();
      bicgstab->atol(1e-12);
      bicgstab->rtol(1e-12);
      bicgstab->stol(1e-12);
      bicgstab->max_it(100000);
      bicgstab->pc_type("hypre");
      bicgstab->verbose(false);

      lsolver_pf = bicgstab;
    } else {

      // lsolver_pf =
      //     std::make_shared<LUDecomposition<PetscMatrix, PetscVector>>();
      auto bicgstab = std::make_shared<BiCGStab<PetscMatrix, PetscVector>>();
      bicgstab->atol(1e-12);
      bicgstab->rtol(1e-12);
      bicgstab->stol(1e-12);
      bicgstab->max_it(100000);
      bicgstab->pc_type("lu");
      bicgstab->verbose(false);

      lsolver_pf = bicgstab;
    }

    auto nlsolver_pf =
        std::make_shared<Newton<utopia::PetscMatrix, utopia::PetscVector>>(
            lsolver_pf);
    nlsolver_pf->atol(1e-7);
    nlsolver_pf->rtol(1e-6);
    nlsolver_pf->stol(1e-8);

    // auto ls_strategy_pf =
    //     std::make_shared<utopia::Backtracking<utopia::PetscVector>>();
    // nlsolver_pf->set_line_search_strategy(ls_strategy_pf);

    if (neP->exact_solve == PETSC_FALSE) {
      nlsolver_pf->forcing_strategy(InexactNewtonForcingStartegies::CAI);
    }

    TwoFieldAlternateMinimization<utopia::PetscMatrix, utopia::PetscVector>
        nlsolver(nlsolver_disp, nlsolver_pf);

    nlsolver.set_field_functions(fun_disp_ptr, fun_pf_ptr);
    nlsolver.set_transfers(neP->disp_to_mono, neP->pf_to_mono,
                           neP->mono_to_disp, neP->mono_to_pf);

    nlsolver.verbose(true);
    nlsolver.atol(snes->abstol);
    nlsolver.rtol(snes->rtol);
    nlsolver.stol(snes->stol);
    nlsolver.max_it(snes->max_its);

    std::cout << "--- Params --- \n";
    std::cout << "atol:   " << snes->abstol << "\n";
    std::cout << "rtol:   " << snes->rtol << "\n";
    std::cout << "stol:   " << snes->stol << "\n";
    std::cout << "max_its:   " << snes->max_its << "\n";

    std::cout << "snes_am_disp_diff_tol:   " << neP->snes_am_disp_diff_tol
              << "\n";
    std::cout << "snes_am_c_diff_tol:   " << neP->snes_am_c_diff_tol << "\n";

    nlsolver.field1_diff_tol(neP->snes_am_disp_diff_tol);
    nlsolver.field2_diff_tol(neP->snes_am_c_diff_tol);

    // nlsolver.verbosity_level(VerbosityLevel::VERBOSITY_LEVEL_VERY_VERBOSE);
    // nlsolver.verbosity_level(VerbosityLevel::VERBOSITY_LEVEL_DEBUG);

    nlsolver.solve(fun_global, uX_global);
    convert(uX_global, snes->vec_sol);

    const auto &sol_status = nlsolver.solution_status();
    sol_status.describe(std::cout);
    snes->reason = SNES_CONVERGED_FNORM_ABS;
    snes->iter = sol_status.global_.iterates;
    snes->linear_its = sol_status.global_.sum_linear_its;

    neP->nl_iterates_global = sol_status.global_.iterates;
    neP->nl_iterates_field1 = sol_status.nl_iterates_field1;
    neP->nl_iterates_field2 = sol_status.nl_iterates_field2;

    neP->num_linear_solves_global = sol_status.global_.num_linear_solves;
    neP->num_linear_solves_field1 = sol_status.num_linear_solves_field1;
    neP->num_linear_solves_field2 = sol_status.num_linear_solves_field2;

    neP->sum_linear_its_global = sol_status.global_.sum_linear_its;
    neP->sum_linear_its_field1 = sol_status.sum_linear_its_field1;
    neP->sum_linear_its_field2 = sol_status.sum_linear_its_field2;

    neP->execution_time = sol_status.global_.execution_time;
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SNESCreate_AlternateMinimization_Solver"
PETSC_EXTERN PetscErrorCode SNESCreate_AlternateMinimization_Solver(SNES snes) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  SNES_AlternateMinimization *neP;

  snes->ops->destroy = SNESDestroy_AlternateMinimization_Solver;
  snes->ops->setup = SNESSetUp_AlternateMinimization_Solver;
  snes->ops->setfromoptions = SNESSetFromOptions_AlternateMinimization_Solver;
  snes->ops->view = SNESView_AlternateMinimization_Solver;
  snes->ops->solve = SNESSolve_AlternateMinimization_Solver;
  snes->ops->reset = SNESReset_AlternateMinimization_Solver;

  snes->usesksp = PETSC_FALSE;
  snes->usesnpc = PETSC_TRUE;

  neP = new SNES_AlternateMinimization();

  snes->data = (void *)neP;

  PetscPrintf(PETSC_COMM_WORLD,
              "In Function SNESCreate_AlternateMinimization_Solver \n");
  PetscFunctionReturn(0);
}

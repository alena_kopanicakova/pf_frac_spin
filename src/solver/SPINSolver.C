
#include "SNESFunction.h"
#include "SPINSolver.h"
#include <petsc/private/snesimpl.h>
#include <petscsnes.h>

/* Our own solver, to be registered at runtime-solver */
#undef __FUNCT__
#define __FUNCT__ "SNESReset_SPIN_Solver"
PetscErrorCode SNESReset_SPIN_Solver(SNES /*snes*/) {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
/*
 SNESDestroy_SPIN_Solver - Destroys the private
 SNES_SPIN_Solver" context that was created with
 SNESCreate_SPIN_Solver"().

 Input Parameter:
 . snes - the SNES context

 Application Interface Routine: SNESDestroy()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESDestroy_SPIN_Solver"
PetscErrorCode SNESDestroy_SPIN_Solver(SNES snes) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = SNESReset_SPIN_Solver(snes);
  CHKERRQ(ierr);

  // SNES_PASSO * passo;
  // passo = (SNES_PASSO *)snes->data;
  // passo->interpolations.clear();
  // delete passo;
  PetscFunctionReturn(0);
}
/*
 SNESSetUp_SPIN_Solver - Sets up the internal data structures for the
 later use of the SNESSetUp_SPIN_Solver nonlinear solver.

 Input Parameters:
 +  snes - the SNES context
 -  x - the solution vector

 Application Interface Routine: SNESSetUp()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESSetUp_SPIN_Solver"
PetscErrorCode SNESSetUp_SPIN_Solver(SNES /*snes*/) {
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
/*
 SNESSetFromOptions__SPIN_Solver - Sets various parameters for the
 SNESSPINLS method.

 Input Parameter:
 . snes - the SNES context

 Application Interface Routine: SNESSetFromOptions()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESSetFromOptions_SPIN_Solver"
static PetscErrorCode
SNESSetFromOptions_SPIN_Solver(PetscOptionItems *PetscOptionsObject,
                               SNES snes) {
  PetscFunctionBegin;

  SNES_SPIN *ctx = (SNES_SPIN *)snes->data;

  PetscOptionsBool("-snes_spin_additive", "additive_flg", "", ctx->additive_flg,
                   &ctx->additive_flg, NULL);

  PetscOptionsBool("-snes_spin_use_switch", "use_switch", "", ctx->use_switch,
                   &ctx->use_switch, NULL);

  PetscOptionsScalar("-snes_spin_action_rtol", "snes_spin_action_rtol", "",
                     ctx->snes_spin_action_rtol, &ctx->snes_spin_action_rtol,
                     NULL);

  PetscOptionsBool("-snes_spin_use_exact_hessian",
                   "snes_spin_use_exact_hessian", "",
                   ctx->snes_spin_use_exact_hessian,
                   &ctx->snes_spin_use_exact_hessian, NULL);

  PetscOptionsBool("-snes_spin_use_block_precond_global",
                   "snes_spin_use_block_precond_global", "",
                   ctx->snes_spin_use_block_precond_global,
                   &ctx->snes_spin_use_block_precond_global, NULL);

  PetscOptionsScalar("-snes_spin_disp_diff_tol", "snes_spin_disp_diff_tol", "",
                     ctx->snes_spin_disp_diff_tol,
                     &ctx->snes_spin_disp_diff_tol, NULL);

  PetscOptionsScalar("-snes_spin_c_diff_tol", "snes_spin_c_diff_tol", "",
                     ctx->snes_spin_c_diff_tol, &ctx->snes_spin_c_diff_tol,
                     NULL);

  PetscFunctionReturn(0);
}
/*
 SNESView_SPIN_Solver - Prints info from the SNES_SPIN_Solver
 data structure.

 Input Parameters:
 + SNES - the SNES context
 - viewer - visualization context

 Application Interface Routine: SNESView()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESView_SPIN_Solver"
static PetscErrorCode SNESView_SPIN_Solver(SNES /*snes*/, PetscViewer viewer) {
  PetscBool iascii;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer, PETSCVIEWERASCII, &iascii);
  CHKERRQ(ierr);
  if (iascii) {
  }
  PetscFunctionReturn(0);
}
/*
 SNESSolve_SPIN_Solver - Solves a nonlinear system with the
 SPIN_Solver method.

 Input Parameters:
 . snes - the SNES context

 Output Parameter:
 . outits - number of iterations until termination

 Application Interface Routine: SNESSolve()
 */
#undef __FUNCT__
#define __FUNCT__ "SNESSolve_SPIN_Solver"
PetscErrorCode SNESSolve_SPIN_Solver(SNES snes) {

  PetscFunctionBegin;
  SNES_SPIN *neP = (SNES_SPIN *)snes->data;

  {
    using namespace utopia;

    Vec X_global = snes->vec_sol;
    Vec X_disp = neP->sub_sness[0]->vec_sol;
    Vec X_pf = neP->sub_sness[1]->vec_sol;

    utopia::PetscVector uX_global, uX_disp, uX_pf;
    convert(X_global, uX_global);
    convert(X_disp, uX_disp);
    convert(X_pf, uX_pf);

    PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector>
        fun_global(snes, uX_global, uX_global);

    PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector> fun_disp(
        neP->sub_sness[0], uX_disp, uX_disp);

    auto fun_disp_ptr = std::make_shared<
        PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector>>(
        fun_disp);

    PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector> fun_pf(
        neP->sub_sness[1], uX_pf, uX_pf);

    auto fun_pf_ptr = std::make_shared<
        PETSCSnesUtopiaInterface<utopia::PetscMatrix, utopia::PetscVector>>(
        fun_pf);

    auto linear_solver_global =
        std::make_shared<KSP_MF<PetscMatrix, PetscVector>>();
    linear_solver_global->ksp_type("gmres");
    linear_solver_global->pc_type("none");
    linear_solver_global->atol(1e-12);
    linear_solver_global->max_it(100);
    linear_solver_global->verbose(false);

    auto lsolver_disp = std::make_shared<BiCGStab<PetscMatrix, PetscVector>>();
    lsolver_disp->atol(1e-12);
    lsolver_disp->rtol(1e-12);
    lsolver_disp->stol(1e-12);
    lsolver_disp->max_it(100000);
    lsolver_disp->pc_type("hypre");
    lsolver_disp->verbose(false);

    KSP ksp_disp = lsolver_disp->implementation();
    PC pc_disp;
    KSPGetPC(ksp_disp, &pc_disp);
    PCHYPRESetType(pc_disp, "boomeramg");
    // PCSetOptionsPrefix(pc_disp, "disp_");
    // PetscOptionsSetValue(NULL, "-disp_pc_hypre_boomeramg_vec_interp_variant",
    //                      "3");

    auto nlsolver_disp =
        std::make_shared<Newton_SPIN<utopia::PetscMatrix, utopia::PetscVector>>(
            lsolver_disp);
    nlsolver_disp->atol(1e-8);
    nlsolver_disp->rtol(1e-6);
    nlsolver_disp->stol(1e-7);
    nlsolver_disp->forcing_strategy(InexactNewtonForcingStartegies::CAI);

    auto lsolver_pf = std::make_shared<BiCGStab<PetscMatrix, PetscVector>>();
    lsolver_pf->atol(1e-12);
    lsolver_pf->stol(1e-12);
    lsolver_pf->rtol(1e-12);
    lsolver_pf->max_it(100000);
    lsolver_pf->pc_type("hypre");

    KSP ksp_pf = lsolver_pf->implementation();
    PC pc_pf;
    KSPGetPC(ksp_pf, &pc_pf);
    PCHYPRESetType(pc_pf, "boomeramg");
    // PCSetOptionsPrefix(pc_disp, "pf_");
    // PetscOptionsSetValue(NULL, "-pf_pc_hypre_boomeramg_vec_interp_variant",
    //                      "1");

    lsolver_pf->verbose(false);

    auto nlsolver_pf =
        std::make_shared<Newton_SPIN<utopia::PetscMatrix, utopia::PetscVector>>(
            lsolver_pf);
    nlsolver_pf->atol(1e-8);
    nlsolver_pf->rtol(1e-6);
    nlsolver_pf->stol(1e-7);

    TwoFieldSPIN<utopia::PetscMatrix, utopia::PetscVector> nlsolver(
        linear_solver_global, nlsolver_disp, nlsolver_pf);

    nlsolver.set_field_functions(fun_disp_ptr, fun_pf_ptr);
    nlsolver.set_transfers(neP->disp_to_mono, neP->pf_to_mono,
                           neP->mono_to_disp, neP->mono_to_pf);

    auto strategy =
        std::make_shared<utopia::CubicBacktracking<utopia::PetscVector>>();
    nlsolver.set_line_search_strategy(strategy);
    nlsolver.forcing_strategy(InexactNewtonForcingStartegies::CAI);

    nlsolver.verbose(true);
    nlsolver.atol(snes->abstol);
    nlsolver.rtol(snes->rtol);
    nlsolver.stol(snes->stol);
    nlsolver.max_it(snes->max_its);
    nlsolver.action_rtol(neP->snes_spin_action_rtol);
    nlsolver.use_switch(neP->use_switch);
    nlsolver.use_exact_hessian(neP->snes_spin_use_exact_hessian);
    nlsolver.use_block_precond_global(neP->snes_spin_use_block_precond_global);

    if (neP->additive_flg) {
      std::cout << "additive SPIN \n";
      nlsolver.additive_precond(true);
    } else {
      std::cout << "multiplicative SPIN \n";
      nlsolver.multiplicative_precond(true);
    }

    std::cout << "--- Params --- \n";
    std::cout << "atol:   " << snes->abstol << "\n";
    std::cout << "rtol:   " << snes->rtol << "\n";
    std::cout << "stol:   " << snes->stol << "\n";
    std::cout << "max_its:   " << snes->max_its << "\n";
    std::cout << "spin_action_rtol:   " << neP->snes_spin_action_rtol << "\n";
    std::cout << "use_switch:   " << neP->use_switch << "\n";

    // std::cout << "snes_spin_disp_diff_tol:   " <<
    // neP->snes_spin_disp_diff_tol
    //           << "\n";
    // std::cout << "snes_spin_c_diff_tol:   " << neP->snes_spin_c_diff_tol
    //           << "\n";

    // nlsolver.verbosity_level(VerbosityLevel::VERBOSITY_LEVEL_VERY_VERBOSE);
    // nlsolver.verbosity_level(VerbosityLevel::VERBOSITY_LEVEL_DEBUG);

    nlsolver.solve(fun_global, uX_global);
    convert(uX_global, snes->vec_sol);

    const auto &sol_status = nlsolver.solution_status();
    sol_status.describe(std::cout);
    snes->reason = SNES_CONVERGED_FNORM_ABS;
    snes->iter = sol_status.global_.iterates;
    snes->linear_its = sol_status.global_.sum_linear_its;

    neP->nl_iterates_global = sol_status.global_.iterates;
    neP->nl_iterates_field1 = sol_status.nl_iterates_field1;
    neP->nl_iterates_field2 = sol_status.nl_iterates_field2;

    neP->num_linear_solves_global = sol_status.global_.num_linear_solves;
    neP->num_linear_solves_field1 = sol_status.num_linear_solves_field1;
    neP->num_linear_solves_field2 = sol_status.num_linear_solves_field2;

    neP->sum_linear_its_global = sol_status.global_.sum_linear_its;
    neP->sum_linear_its_field1 = sol_status.sum_linear_its_field1;
    neP->sum_linear_its_field2 = sol_status.sum_linear_its_field2;

    neP->cond_number_precond = sol_status.cond_number_precond;
    neP->cond_number_unprecond = sol_status.cond_number_unprecond;

    neP->execution_time = sol_status.global_.execution_time;
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SNESCreate_SPIN_Solver"
PETSC_EXTERN PetscErrorCode SNESCreate_SPIN_Solver(SNES snes) {
  PetscErrorCode ierr;

  PetscFunctionBegin;

  SNES_SPIN *neP;

  snes->ops->destroy = SNESDestroy_SPIN_Solver;
  snes->ops->setup = SNESSetUp_SPIN_Solver;
  snes->ops->setfromoptions = SNESSetFromOptions_SPIN_Solver;
  snes->ops->view = SNESView_SPIN_Solver;
  snes->ops->solve = SNESSolve_SPIN_Solver;
  snes->ops->reset = SNESReset_SPIN_Solver;

  snes->usesksp = PETSC_FALSE;
  snes->usesnpc = PETSC_TRUE;

  neP = new SNES_SPIN();

  snes->data = (void *)neP;
  neP->additive_flg = PETSC_TRUE;

  PetscPrintf(PETSC_COMM_WORLD, "In Function SNESCreate_SPIN_Solver \n");
  PetscFunctionReturn(0);
}

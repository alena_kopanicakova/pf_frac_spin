#include "IrreversibilityPenaltyEnergy.h"
#include "libmesh/utility.h"

registerMooseObject("PFfracApp", IrreversibilityPenaltyEnergy);

template <> InputParameters validParams<IrreversibilityPenaltyEnergy>() {
  InputParameters params = validParams<Material>();
  params.addClassDescription("Phase-field fracture energy");
  params.addRequiredCoupledVar("c", "Order parameter for damage");
  params.addRequiredParam<Real>("alpha", "penalty parameter");

  return params;
}

IrreversibilityPenaltyEnergy::IrreversibilityPenaltyEnergy(
    const InputParameters &parameters)
    : Material(parameters), _c(coupledValue("c")), _c_old(coupledValueOld("c")),
      _irr_energy(declareProperty<Real>("irr_energy")),
      _alpha(getParam<Real>("alpha")) {}

void IrreversibilityPenaltyEnergy::initQpStatefulProperties() {}

void IrreversibilityPenaltyEnergy::computeQpProperties() {

  auto c_cold_minus = _c[_qp] - _c_old[_qp];
  auto c_bracket_ = std::min(c_cold_minus, 0.0);

  _irr_energy[_qp] = 0.5 * _alpha * (c_bracket_ * c_bracket_);
}

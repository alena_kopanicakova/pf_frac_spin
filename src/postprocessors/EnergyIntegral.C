#include "EnergyIntegral.h"

registerMooseObject("PFfracApp", EnergyIntegral);

template <> InputParameters validParams<EnergyIntegral>() {
  InputParameters params = validParams<ElementIntegralPostprocessor>();

  params.addRequiredParam<std::string>("energy",
                                       "Type of energy to be outputed.");

  return params;
}

EnergyIntegral::EnergyIntegral(const InputParameters &parameters)
    : ElementIntegralPostprocessor(parameters),
      _energy(
          getMaterialProperty<Real>(parameters.get<std::string>("energy"))) {}

Real EnergyIntegral::computeQpIntegral() { return _energy[_qp]; }
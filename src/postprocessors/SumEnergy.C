#include "PFfracApp.h"
#include "SumEnergy.h"

registerMooseObject("PFfracApp", EnergySum);

template <> InputParameters validParams<EnergySum>() {
  InputParameters params = validParams<GeneralPostprocessor>();
  params.addParam<std::vector<PostprocessorName>>(
      "energies", "The names of energies to be sumed up.");

  return params;
}

EnergySum::EnergySum(const InputParameters &parameters)
    : GeneralPostprocessor(parameters),
      _energy_names(getParam<std::vector<PostprocessorName>>("energies")) {
  for (auto i = 0; i < _energy_names.size(); i++)
    _energies.push_back(
        std::ref(getPostprocessorValueByName(_energy_names[i])));
}

EnergySum::~EnergySum() {}

void EnergySum::initialize() {}

void EnergySum::execute() {
  _value = 0.0;

  for (auto i = 0; i < _energy_names.size(); i++)
    _value += _energies[i].get();
}

Real EnergySum::getValue() { return _value; }
#include "DisplacedProblem.h"
#include "FEProblem.h"
#include "Factory.h"
#include "MooseMesh.h"
#include "MooseUtils.h"
#include "MultiappAssembler.h"
#include "MultiappFEProblem.h"
#include "NonlinearSystem.h"
#include "PFTransient.h"

#include <libmesh/parallel_mesh.h>
#include <libmesh/petsc_matrix.h>
#include <libmesh/petsc_nonlinear_solver.h>
// libmesh includes
#include "libmesh/nonlinear_solver.h"
#include "libmesh/petsc_matrix.h"
#include "libmesh/petsc_nonlinear_solver.h"
#include "libmesh/sparse_matrix.h"

registerMooseObject("PFfracApp", MultiappFEProblem);

template <> InputParameters validParams<MultiappFEProblem>() {
  InputParameters params = validParams<FEProblem>();
  params.addParam<std::string>(
      "multilevel_multiapp", "",
      "Name of multiapp with multilevel assembly routines.");
  params.addParam<bool>("auto_dt", false, "Is dt adjusted during solve ? ");
  return params;
}

MultiappFEProblem::MultiappFEProblem(const InputParameters &parameters)
    : FEProblem(parameters),
      _multilevel_multiapp(parameters.get<std::string>("multilevel_multiapp")),
      _auto_dt(parameters.get<bool>("auto_dt")) {}
MultiappFEProblem::~MultiappFEProblem() {}

void MultiappFEProblem::solve() {

  clearAllDofIndices();
  if (_displaced_problem)
    _displaced_problem->clearAllDofIndices();

  Moose::PetscSupport::petscSetOptions(*this);

  // set up DM which is required if use a field split preconditioner
  // We need to setup DM every "solve()" because libMesh destroy SNES after
  // solve() Do not worry, DM setup is very cheap
  if (_nl->haveFieldSplitPreconditioner())
    Moose::PetscSupport::petscSetupDM(*_nl);

  Moose::setSolverDefaults(*this);
  // Setup the output system for printing linear/nonlinear iteration information
  initPetscOutput();
  possiblyRebuildGeomSearchPatches();

  if (_solve) {

    if (_multilevel_multiapp != "") {

      // // TODO:: pass nullspaces, pass preconditioner
      PetscNonlinearSolver<Number> *petsc_solver =
          dynamic_cast<PetscNonlinearSolver<Number> *>(_nl->nonlinearSolver());
      SNES _snes = petsc_solver->snes();

      auto multiapp = this->getMultiApp(_multilevel_multiapp);
      // here, we can do other multiapps, not just multilevel assembly
      if ((dynamic_cast<MultiappAssembler *>(multiapp.get()))) {
        auto my_multiapp = dynamic_cast<MultiappAssembler *>(multiapp.get());
        // needed - in case that step is rejected and step shrinked
        if (dynamic_cast<PFTransient *>(_app.getExecutioner())) {
          auto ex_transient_passo =
              dynamic_cast<PFTransient *>(_app.getExecutioner());
          my_multiapp->pre_process(this->time(),
                                   ex_transient_passo->get_current_step(),
                                   ex_transient_passo->get_dt());
        } else {
          my_multiapp->pre_process(this->time());
        }

        my_multiapp->make_assembler_ready(_snes);

        // finally, going to solver
        _nl->solve();
        my_multiapp->post_process();
      } else {
        _nl->solve();
      }
    } else {
      std::cout << "MultiappFEProblem, one level solve ! \n";
      _nl->solve();
      // }
      ///////////////////////////////////// multilevel things done
      ////////////////////////////////////////
    }
    if (_solve) {
      _nl->update();
    }
    // sync solutions in displaced problem
    if (_displaced_problem)
      _displaced_problem->syncSolutions();
  }
}

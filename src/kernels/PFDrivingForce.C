#include "MathUtils.h"
#include "PFDrivingForce.h"
#include "RankTwoTensor.h"

registerMooseObject("PFfracApp", PFDrivingForce);

template <> InputParameters validParams<PFDrivingForce>() {
  InputParameters params = validParams<Kernel>();
  params.addClassDescription("Kernel to compute bulk energy contribution to "
                             "damage order parameter residual equation");
  params.addRequiredParam<MaterialPropertyName>(
      "G0", "Material property name with undamaged strain energy driving "
            "damage (G0_pos)");
  params.addParam<MaterialPropertyName>(
      "dG0_dstrain",
      "Material property name with derivative of G0_pos with strain");
  params.addCoupledVar("displacements",
                       "The displacements appropriate for the simulation "
                       "geometry and coordinate system");

  params.addParam<Real>("alpha", 1, "Param deg. function");
  params.addParam<Real>("beta", 1, "Param deg. function");
  params.addParam<Real>("gamma", 1, "Param deg. function");
  params.addParam<Real>("delta", 1, "Param deg. function");
  params.addParam<Real>("alpha_star", 2, "Param deg. function");

  return params;
}

PFDrivingForce::PFDrivingForce(const InputParameters &parameters)
    : Kernel(parameters), _G0_pos(getMaterialProperty<Real>("G0")),
      _alpha(getParam<Real>("alpha")), _beta(getParam<Real>("beta")),
      _gamma(getParam<Real>("gamma")), _delta(getParam<Real>("delta")),
      _alpha_star(getParam<Real>("alpha_star")),
      _dG0_pos_dstrain(isParamValid("dG0_dstrain")
                           ? &getMaterialProperty<RankTwoTensor>("dG0_dstrain")
                           : NULL),
      _ndisp(coupledComponents("displacements")), _disp_var(_ndisp) {
  for (unsigned int i = 0; i < _ndisp; ++i)
    _disp_var[i] = coupled("displacements", i);
}

Real PFDrivingForce::computeQpResidual() {
  const Real c = _u[_qp];

  // derivative of E wrt c
  // return 2.*_G0_pos[_qp] * _u[_qp] * _test[_i][_qp] - 2. * _G0_pos[_qp] *
  // _test[_i][_qp];

  // new version
  // Real c1 = 1.0 - c;
  Real deg_func_dc = (-3.0 * _alpha_star * c * c) + (4.0 * _alpha_star * c) -
                     _alpha_star + (6.0 * c * c) - (6.0 * c);
  return deg_func_dc * _G0_pos[_qp] * _test[_i][_qp];
}

Real PFDrivingForce::computeQpJacobian() {
  // derivative of E wrt c,c
  // return 2. * _G0_pos[_qp] * _phi[_j][_qp] * _test[_i][_qp];

  const Real c = _u[_qp];

  // Real c1 = 1.0 - c;
  Real deg_func_dcc =
      (-6.0 * _alpha_star * c) + (4.0 * _alpha_star) + (12.0 * c) - 6.0;
  return deg_func_dcc * _G0_pos[_qp] * _phi[_j][_qp] * _test[_i][_qp];
}

Real PFDrivingForce::computeQpOffDiagJacobian(unsigned int jvar) {
  unsigned int c_comp;
  bool disp_flag = false;

  // Contribution of auxiliary variable to off diag Jacobian of c
  for (unsigned int k = 0; k < _ndisp; ++k) {
    if (jvar == _disp_var[k]) {
      c_comp = k;
      disp_flag = true;
    } else
      return 0.0;
  }

  // Contribution of displacements to off diag Jacobian of c
  if (disp_flag && _dG0_pos_dstrain != NULL) {
    Real val = (((*_dG0_pos_dstrain)[_qp].column(c_comp) +
                 (*_dG0_pos_dstrain)[_qp].row(c_comp)) /
                2.0) *
               _grad_phi[_j][_qp];
    return val * _test[_i][_qp];
  }

  return 0.0;
}

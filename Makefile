###############################################################################
###############################################################################
PF_FRAC_DIR        ?= $(shell pwd)
MOOSE_DIR          ?= $(shell dirname $(MOOSE_DIR))
UTOPIA_DIR		   ?= $(shell dirname $(UTOPIA_DIR))
# print info 
$(info PF_FRAC_DIR is $(PF_FRAC_DIR))
$(info MOOSE_DIR is $(MOOSE_DIR))
$(info UTOPIA_DIR is $(UTOPIA_DIR))


MODULE_DIR         ?= $(MOOSE_DIR)/modules
FRAMEWORK_DIR      ?= $(MOOSE_DIR)/framework
SRC_DIR	           ?= $(PF_FRAC_DIR)/src
###############################################################################
# framework
include $(FRAMEWORK_DIR)/build.mk
include $(FRAMEWORK_DIR)/moose.mk


################################## MODULES ################################
ALL_MODULES       := yes
# INCLUDE_COMBINED  := no
USE_TEST_LIBS     := yes
SKIP_LOADER       := yes
include $(MODULE_DIR)/modules.mk


# dep apps
APPLICATION_DIR    := $(PF_FRAC_DIR)
APPLICATION_NAME   := pf_frac
BUILD_EXEC         := yes
DEP_APPS           := $(shell $(FRAMEWORK_DIR)/scripts/find_dep_apps.py $(APPLICATION_NAME))
include            $(FRAMEWORK_DIR)/app.mk


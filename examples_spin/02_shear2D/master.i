# mpirun -n 4  ../../pf_frac-opt -i master.i Outputs/file_base=tension_SPIN_multiplicative_rtol1e2_adaptive_atol1e1  -snes_spin_additive false  -snes_atol 1e-6 -snes_rtol 1e-7 -snes_stol 1e-9 -snes_max_it 300 -snes_spin_action_rtol 1e-2
[Mesh]
  type = FileMesh
  file = /home/kothari/git_repos/moose_pf/pf_frac_spin/meshes/crack_mesh_shear2.e
  # uniform_refine = 1
  use_displaced_mesh = false
  parallel_type = distributed
[]

[GlobalParams]
  displacements = 'disp_x disp_y'
  ls = 0.006
  alpha_star = 2
  kdamage = 1e-9
  gc = 0.0027
  alpha=4.5e3
[]

[Problem]
  type = MultiappFEProblem
  solve = true
  kernel_coverage_check = false
  multilevel_multiapp = full_solve
[]

[Modules]
  [./TensorMechanics]
    [./Master]
      [./mech]
        add_variables = true
        strain = SMALL
      [../]
    [../]
  [../]
[]

[Variables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
  [./c]
  [../]
[]

[Functions]
  [./tfunc]
    type = ParsedFunction
    value = '1 * t'
  [../]
[]


[Kernels]
  [./pf_frac]
    type = PFSubproblem
    variable = c
    gc = 'gc_prop'
    driving_force = 'G0_pos'
    dG0_dstrain = 'dG0_pos_dstrain'
    # use_off_diag_terms = false
  [../]
  [./solid_x]
    type = PhaseFieldFractureMechanicsOffDiag
    variable = disp_x
    component = 0
    c = c
  [../]
  [./solid_y]
    type = PhaseFieldFractureMechanicsOffDiag
    variable = disp_y
    component = 1
    c = c
  [../]
  [./pf_frac_irreversibility]
    type = PFIrreversibilityPenalty
    variable = c
    use_old = True
  [../]  
[]



[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '121.15 80.77'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]
  [./PF_energy_mat]
    type = FracEnergy
    c = c
  [../]
  [./irr_energy]
    type = IrreversibilityPenaltyEnergy
    c = c
  [../]
[]

[Postprocessors]
  [./NlItsGlobalAcum]
    type = SPINStats
    quantity='nl_iterates_global'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./NlItsDispAcum]
    type = SPINStats
    quantity='nl_iterates_field1'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./NlItsPfAcum]
    type = SPINStats
    quantity='nl_iterates_field2'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsGlobalAcum]
    type = SPINStats
    quantity='sum_linear_its_global'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsDispAcum]
    type = SPINStats
    quantity='sum_linear_its_field1'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsPfAcum]
    type = SPINStats
    quantity='sum_linear_its_field2'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  # [./elastic]
  #   execute_on = 'linear'
  #   energy     = elastic_energy
  #   type = EnergyIntegral
  # [../]
  # [./energy_pf]
  #   execute_on = 'linear'
  #   energy     = PF_energy
  #   type = EnergyIntegral
  # [../]
  # [./energy_pf_irr]
  #   execute_on = 'linear'
  #   energy     = irr_energy
  #   type = EnergyIntegral
  # [../]
  # [./energy_sum]
  #   execute_on          = linear
  #   energies ='elastic energy_pf energy_pf_irr'
  #   type                = EnergySum
  # [../]
  # [./elastic]
  #   execute_on = 'linear'
  #   energy     = elastic_energy
  #   type = EnergyIntegral
  # [../]
  # [./energy_frac_irr]
  #   execute_on          = linear
  #   energy_frac ='PF_energy' 
  #   energy_irr = 'irr_energy'
  #   type  = FracIrrEnergyIntegral
  # [../]  
  # [./energy_sum]
  #   execute_on          = linear
  #   # energies ='elastic energy_pf energy_pf_irr'
  #   energies ='elastic energy_frac_irr'
  #   type                = EnergySum
  # [../]  
    [./energy_sum]
    execute_on          = linear
    energy_frac ='PF_energy' 
    energy_irr = 'irr_energy'
    energy_elastic = 'elastic_energy'
    type  = TotalEnergyIntegral
  [../] 
  [./ExecTime]
    type = SPINStats
    quantity='execution_time'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
[] # Postprocessors

[BCs]
  [./ydisp]
    type = FunctionDirichletBC
    variable = disp_x
    boundary = 2
    function = tfunc
    preset=true
  [../]
  [./yfix]
    type = DirichletBC
    variable = disp_x
    boundary = 1
    preset=true
    value = 0
  [../]
  [./xfix]
    type = DirichletBC
    variable = disp_y
    boundary = '1 2'
    value = 0
    preset=true
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = PFTransient
  solve_type=Newton

  solver_type = 'ut_SPIN'

  petsc_options_iname = '-snes_spin_additive'  # works on command line, not here
  petsc_options_value = 'true'

  energy_postprocessor = energy_sum
  end_time=0.038
  
  [./TimeStepper]
    type = TimeChangeConstantDT
    dt = 1e-3
    switch_time = 0.008
    dt_after_switch = 7.5e-5
  [../]
[]

[MultiApps]
  [./full_solve]
    type = MultiappAssembler
    app_type = PFfracApp
    execute_on = 'timestep_begin'
    positions = '0 0 0 0 0 0'
    input_files = 'sub_disp.i sub_pf.i'
    # clone_master_mesh = true
  [../]
[]

[Transfers]
  [./to_sub]
    type = MultiAppCopyTransfer
    direction = to_multiapp
    source_variable = c
    variable = c
    multi_app = full_solve
    execute_on = 'timestep_end'
  [../]
[]

[Outputs]
  exodus = true
  csv = true
  file_base = "shear_SPIN"
[]
[Mesh]
  type = FileMesh
  file = ../../../meshes/L_shape_non_uniform.e
  parallel_type = distributed
  use_displaced_mesh = false
  uniform_refine=0
[]

[GlobalParams]
  displacements = 'disp_x disp_y'
  alpha_star = 2
  kdamage = 1e-9
  gc = 8.9e-5
[]

[Problem]
  type = MultiappFEProblem
  solve = false
  kernel_coverage_check = false
[]

[Modules]
  [./TensorMechanics]
    [./Master]
      [./mech]
        add_variables = true
        strain = SMALL
      [../]
    [../]
  [../]
[]



[Variables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
[]


[AuxVariables]
  [./c]
  [../]
[]


[Functions]
  [./tfunc]
    type = ParsedFunction
    value = '1 * t'
    # value = 'if(t<=0.3, t, if(t<=0.8, 0.6-t,  t-0.8))'
  [../]
[]




[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '6.16 10.95'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]
[]



[BCs]
  [./x_bottom]
    type = DirichletBC
    variable = disp_x
    boundary = 1
    preset=true
    value = 0
  [../]
  [./y_bottom]
    type = DirichletBC
    variable = disp_y
    boundary = 1
    value = 0
    preset=true
  [../]
  [./y_side]
    type = FunctionDirichletBC
    variable = disp_y
    boundary = 3
    function = tfunc
    preset=true
  [../]  
[]


[Postprocessors]
  [./elastic_energy]
    execute_on = 'linear'
    energy     = elastic_energy
    type = EnergyIntegral
  [../]
[] # Postprocessors



[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = PFTransient
  solver_type = 'ut_SPIN'
    solve_type=Newton


  petsc_options_iname = '-snes_spin_additive'
  petsc_options_value = 'false'



  energy_postprocessor = elastic_energy

  # petsc_options_iname = '-snes_max_it -passo_atol -passo_stol -passo_rtol -snes_tr_alg -snes_tr_delta -snes_tr_gamma1 -snes_tr_local_max_iterate -passo_verbose -passo_tr_alg -passo_ksp_atol -snes_atol'
  # petsc_options_value = '1000 1E-7 1E-7 1E-9 20000000 100000000000 0.2 10 true STEIHAUG_TOINT 1e-15                  1e-12'
  num_steps = 1
[]

[Outputs]
  exodus = false
  csv = false
[]

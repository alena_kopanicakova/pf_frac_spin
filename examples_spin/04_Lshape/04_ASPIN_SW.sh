#!/bin/bash -l
#SBATCH --time=48:00:00 
#SBATCH --output=04_ASPIN_SW.out
#SBATCH --error=04_ASPIN_SW.err 
#SBATCH --nodes=1 
#SBATCH --mem=63300
#SBATCH --ntasks-per-node=20

OUTPUT=Outputs/file_base=04_ASPIN_SW
SOLVER=Executioner/solver_type='ut_SPIN'

PF_SPIN_DIR=/home/kopanicakova/MOOSE_01_09_2021/pf_frac_moose
MESH=Mesh/file=$PF_SPIN_DIR/meshes/L_shape_non_uniform.e
MESH_SUB0=full_solve0:Mesh/file=$PF_SPIN_DIR/meshes/L_shape_non_uniform.e
MESH_SUB1=full_solve1:Mesh/file=$PF_SPIN_DIR/meshes/L_shape_non_uniform.e

INPUT_FILE=$PF_SPIN_DIR/examples_spin/04_Lshape/master.i

ADDITIVE_SPIN=true
EXACT_HESSIAN=false
USE_SWITCH=true
BLOCK_PREC_GLOBAL=true

SNES_RTOL=1e-6
SNES_ATOL=1e-7
SNES_STOL=1e-8

SNES_SUB_RTOL=1e-6
SNES_SUB_ATOL=1e-7
SNES_SUB_STOL=1e-8

DISP_DIFF_TOL=-1e-12
C_DIFF_TOL=-1e-12

MAXIT=50000
SPIN_ACTION_RTOL=1e-4

echo "mpirun ${PF_SPIN_DIR}/pf_frac-opt -i $INPUT_FILE $OUTPUT $MESH $MESH_SUB0 $MESH_SUB1 $SOLVER -snes_spin_additive $ADDITIVE_SPIN -snes_spin_use_exact_hessian $EXACT_HESSIAN -snes_spin_use_block_precond_global $BLOCK_PREC_GLOBAL -snes_spin_use_switch $USE_SWITCH -snes_atol $SNES_ATOL -snes_rtol $SNES_RTOL -snes_stol $SNES_STOL -snes_sub_atol $SNES_SUB_ATOL -snes_sub_rtol $SNES_SUB_RTOL -snes_sub_stol $SNES_SUB_STOL -snes_max_it $MAXIT -snes_spin_action_rtol $SPIN_ACTION_RTOL -snes_spin_disp_diff_tol $DISP_DIFF_TOL -snes_spin_c_diff_tol $C_DIFF_TOL"

mpirun ${PF_SPIN_DIR}/pf_frac-opt -i $INPUT_FILE $OUTPUT $MESH $MESH_SUB0 $MESH_SUB1 $SOLVER -snes_spin_additive $ADDITIVE_SPIN -snes_spin_use_exact_hessian $EXACT_HESSIAN -snes_spin_use_block_precond_global $BLOCK_PREC_GLOBAL -snes_spin_use_switch $USE_SWITCH -snes_atol $SNES_ATOL -snes_rtol $SNES_RTOL -snes_stol $SNES_STOL -snes_sub_atol $SNES_SUB_ATOL -snes_sub_rtol $SNES_SUB_RTOL -snes_sub_stol $SNES_SUB_STOL -snes_max_it $MAXIT -snes_spin_action_rtol $SPIN_ACTION_RTOL -snes_spin_disp_diff_tol $DISP_DIFF_TOL -snes_spin_c_diff_tol $C_DIFF_TOL

#   mpirun -n 4  ../../pf_frac-opt -i master_alternate_min.i  -snes_atol 1e-6 -snes_rtol 1e-7 -snes_stol 1e-9 -snes_max_it 300
#   mpirun -n 4  ../../pf_frac-opt -i master_alternate_min.i  Outputs/file_base=tension_AM_exact_solves  -snes_atol 1e-6 -snes_rtol 1e-7 -snes_stol 1e-9 -snes_max_it 300
[Mesh]
  type = FileMesh
  file = ../../../meshes/L_shape_non_uniform.e
  parallel_type = distributed
  use_displaced_mesh = false
  uniform_refine=0
[]

[GlobalParams]
  displacements = 'disp_x disp_y'
  ls = 2.
  gc = 8.9e-5  
  alpha_star = 2
  kdamage = 1e-9
  alpha = 4.5
[]

[Problem]
  type = MultiappFEProblem
  solve = true
  kernel_coverage_check = false
  multilevel_multiapp = full_solve
[]

[Modules]
  [./TensorMechanics]
    [./Master]
      [./mech]
        add_variables = true
        strain = SMALL
      [../]
    [../]
  [../]
[]


[Variables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
  [./c]
  [../]
[]


[Functions]
  [./tfunc]
    type = ParsedFunction
    value = '1 * t'
    # value = 'if(t<=0.3,t,if(t<=0.8, 0.6-t,t-0.8))'
  [../]
[]


[Kernels]
  [./pf_frac]
    type = PFSubproblem
    variable = c
    gc = 'gc_prop'
    driving_force = 'G0_pos'
    dG0_dstrain = 'dG0_pos_dstrain'
    # use_off_diag_terms = false
  [../]
  [./solid_x]
    type = PhaseFieldFractureMechanicsOffDiag
    variable = disp_x
    component = 0
    c = c
  [../]
  [./solid_y]
    type = PhaseFieldFractureMechanicsOffDiag
    variable = disp_y
    component = 1
    c = c
  [../]
  [./pf_frac_irreversibility]
    type = PFIrreversibilityPenalty
    variable = c
    use_old = True
  [../]
[]



[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '6.16 10.95'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]
  [./PF_energy_mat]
    type = FracEnergy
    c = c
  [../]
  [./irr_energy]
    type = IrreversibilityPenaltyEnergy
    c = c
  [../]  
[]

[Postprocessors]
  [./NlItsGlobalAcum]
    type = SPINStats
    quantity='nl_iterates_global'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./NlItsDispAcum]
    type = SPINStats
    quantity='nl_iterates_field1'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./NlItsPfAcum]
    type = SPINStats
    quantity='nl_iterates_field2'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsGlobalAcum]
    type = SPINStats
    quantity='sum_linear_its_global'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsDispAcum]
    type = SPINStats
    quantity='sum_linear_its_field1'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsPfAcum]
    type = SPINStats
    quantity='sum_linear_its_field2'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./ExecTime]
    type = SPINStats
    quantity='execution_time'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]  
    [./energy_sum]
    execute_on          = linear
    energy_frac ='PF_energy' 
    energy_irr = 'irr_energy'
    energy_elastic = 'elastic_energy'
    type  = TotalEnergyIntegral
  [../] 
[] # Postprocessors


[BCs]
  [./x_bottom]
    type = DirichletBC
    variable = disp_x
    boundary = 1
    preset=true
    value = 0
  [../]
  [./y_bottom]
    type = DirichletBC
    variable = disp_y
    boundary = 1
    value = 0
    preset=true
  [../]
  [./y_side]
    type = FunctionDirichletBC
    variable = disp_y
    boundary = 3
    function = tfunc
    preset=true
  [../]  
[]


[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = PFTransient
  solve_type=Newton

  solver_type = 'ut_SPIN'

  petsc_options_iname = '-snes_spin_additive'  # works on command line, not here
  petsc_options_value = 'true'


  energy_postprocessor = energy_sum
  end_time = 0.8
  
  [./TimeStepper]
    type = TimeChangeConstantDT
    dt = 1e-2
    switch_time = 0.35
    dt_after_switch = 1e-3
  [../]
[]


[MultiApps]
  [./full_solve]
    type = MultiappAssembler
    app_type = PFfracApp
    execute_on = 'timestep_begin'
    positions = '0 0 0 0 0 0'
    input_files = 'sub_disp.i sub_pf.i'
  [../]
[]


[Transfers]
  [./to_sub]
    type = MultiAppCopyTransfer
    direction = to_multiapp
    source_variable = c
    variable = c
    multi_app = full_solve
    execute_on = 'timestep_end'
  [../]
[]


[Outputs]
  exodus = true
  csv = true
  file_base = "Lshape_alternate_min"
[]


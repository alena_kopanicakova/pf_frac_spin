[Mesh]
  type = FileMesh
  file = /home/kothari/git_repos/moose_pf/pf_frac_spin/meshes/three_point_bending2D_ref.e
  uniform_refine = 1
  use_displaced_mesh = false
  parallel_type = distributed
[]

[GlobalParams]
  alpha_star = 2
  kdamage = 1e-9
  ls = 0.02
  gc = 5.4e-4
  alpha = 270.
[]

[Problem]
  type = MultiappFEProblem
  solve = false
  kernel_coverage_check = false
[]

[Variables]
  [./c]
  [../]
[]

[AuxVariables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
[]

[Kernels]
  [./pf_frac]
    type = PFSubproblem
    variable = c
    gc = 'gc_prop'
    driving_force = 'G0_pos'
    dG0_dstrain = 'dG0_pos_dstrain'
  [../]
  [./pf_frac_irreversibility]
    type = PFIrreversibilityPenalty
    variable = c
    use_old = True
  [../]   
[]

[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '12.0 8.0'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]
  [./PF_energy_mat]
    type = FracEnergy
    c = c
  [../]
  [./strain]
    type = ComputeSmallStrain
    displacements='disp_x disp_y'
  [../]
  [./irr_energy]
    type = IrreversibilityPenaltyEnergy
    c = c
  [../] 
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Postprocessors]
  [./energy_sum]
    execute_on          = linear
    energy_frac ='PF_energy' 
    energy_irr = 'irr_energy'
    type  = FracIrrEnergyIntegral
  [../] 
[] # Postprocessors

[Executioner]
  type = PFTransient
  solver_type = 'ut_SPIN'
    solve_type=Newton

  petsc_options_iname = '-snes_spin_additive'
  petsc_options_value = 'false'

  energy_postprocessor = energy_sum

  # petsc_options_iname = '-snes_max_it -passo_atol -passo_stol -passo_rtol -snes_tr_alg -snes_tr_delta -snes_tr_gamma1 -snes_tr_local_max_iterate -passo_verbose -passo_tr_alg -passo_ksp_atol -snes_atol'
  # petsc_options_value = '1000 1E-7 1E-7 1E-9 20000000 100000000000 0.2 10 true STEIHAUG_TOINT 1e-15                  1e-12'
  num_steps = 1
[]

[Outputs]
  exodus = false
  csv = false
[]

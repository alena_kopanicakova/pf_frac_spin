[Mesh]
  type = FileMesh
  file = /home/kothari/git_repos/moose_pf/pf_frac_spin/meshes/three_point_bending2D_ref.e
  uniform_refine = 2
  use_displaced_mesh = false
  parallel_type = distributed
[]

[GlobalParams]
  displacements = 'disp_x disp_y'
  alpha_star = 2
  kdamage = 1e-9
  ls = 0.01
  gc = 5.4e-4
  alpha = 540.
[]

[Problem]
  type = MultiappFEProblem
  solve = false
  kernel_coverage_check = false
[]

[Modules]
  [./TensorMechanics]
    [./Master]
      [./mech]
        add_variables = true
        strain = SMALL
      [../]
    [../]
  [../]
[]

[Variables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
[]

[AuxVariables]
  [./c]
  [../]
[]

[Functions]
  [./tfunc]
    type = ParsedFunction
    value = '-1. * t'
  [../]
[]

[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '12.0 8.0'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]
[]

[Postprocessors]
  [./elastic_energy]
    execute_on = 'linear'
    energy     = elastic_energy
    type = EnergyIntegral
  [../]
[] # Postprocessors

[BCs]
  [./x_bottomleft]
    type = DirichletBC
    variable = disp_x
    boundary = 1
    preset=true
    value = 0
  [../]
  [./y_bottomleft]
    type = DirichletBC
    variable = disp_y
    boundary = 1
    value = 0
    preset=true
  [../]
  [./y_bottomright]
    type = DirichletBC
    variable = disp_y
    boundary = 2
    value = 0
    preset=true
  [../]
  [./ydispfun]
    type = FunctionDirichletBC
    variable = disp_y
    boundary = 3
    function = tfunc
    preset=true
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = PFTransient
  solver_type = 'ut_SPIN'
    solve_type=Newton

  petsc_options_iname = '-snes_spin_additive'
  petsc_options_value = 'false'

  energy_postprocessor = elastic_energy
  # petsc_options_iname = '-snes_max_it -passo_atol -passo_stol -passo_rtol -snes_tr_alg -snes_tr_delta -snes_tr_gamma1 -snes_tr_local_max_iterate -passo_verbose -passo_tr_alg -passo_ksp_atol -snes_atol'
  # petsc_options_value = '1000 1E-7 1E-7 1E-9 20000000 100000000000 0.2 10 true STEIHAUG_TOINT 1e-15                  1e-12'
  num_steps = 1
[]

[Outputs]
  exodus = false
  csv = false
[]

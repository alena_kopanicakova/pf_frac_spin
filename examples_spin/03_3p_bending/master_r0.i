# mpirun -n 4  ../../pf_frac-opt -i master.i Outputs/file_base=tension_SPIN_multiplicative_rtol1e2_adaptive_atol1e1  -snes_spin_additive false  -snes_atol 1e-6 -snes_rtol 1e-7 -snes_stol 1e-9 -snes_max_it 300 -snes_spin_action_rtol 1e-2
[Mesh]
  type = FileMesh
  file = /home/kothari/git_repos/moose_pf/pf_frac_spin/meshes/three_point_bending2D_ref.e
  uniform_refine = 0
  use_displaced_mesh = false
  parallel_type = distributed
[]

[GlobalParams]
  displacements = 'disp_x disp_y'
  alpha_star = 2
  kdamage = 1e-9
  ls = 0.04
  gc = 5.4e-4
  alpha = 135
[]

[Problem]
  type = MultiappFEProblem
  solve = true
  kernel_coverage_check = false
  multilevel_multiapp = full_solve
[]

[Modules]
  [./TensorMechanics]
    [./Master]
      [./mech]
        add_variables = true
        strain = SMALL
      [../]
    [../]
  [../]
[]

[Variables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
  [./c]
  [../]
[]

[Functions]
  [./tfunc]
    type = ParsedFunction
    value = '-1. * t'
  [../]
[]

[Kernels]
  [./pf_frac]
    type = PFSubproblem
    variable = c
    gc = 'gc_prop'
    driving_force = 'G0_pos'
    dG0_dstrain = 'dG0_pos_dstrain'
    # use_off_diag_terms = false
  [../]
  [./solid_x]
    type = PhaseFieldFractureMechanicsOffDiag
    variable = disp_x
    component = 0
    c = c
  [../]
  [./solid_y]
    type = PhaseFieldFractureMechanicsOffDiag
    variable = disp_y
    component = 1
    c = c
  [../]
  [./pf_frac_irreversibility]
    type = PFIrreversibilityPenalty
    variable = c
    use_old = True
  [../]   
[]

[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '12.0 8.0'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]
  [./PF_energy_mat]
    type = FracEnergy
    c = c
  [../]
  [./irr_energy]
    type = IrreversibilityPenaltyEnergy
    c = c
  [../]  
[]

[Postprocessors]
  [./NlItsGlobalAcum]
    type = SPINStats
    quantity='nl_iterates_global'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./NlItsDispAcum]
    type = SPINStats
    quantity='nl_iterates_field1'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./NlItsPfAcum]
    type = SPINStats
    quantity='nl_iterates_field2'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsGlobalAcum]
    type = SPINStats
    quantity='sum_linear_its_global'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsDispAcum]
    type = SPINStats
    quantity='sum_linear_its_field1'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
  [./LinItsPfAcum]
    type = SPINStats
    quantity='sum_linear_its_field2'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
    [./energy_sum]
    execute_on          = linear
    energy_frac ='PF_energy' 
    energy_irr = 'irr_energy'
    energy_elastic = 'elastic_energy'
    type  = TotalEnergyIntegral
  [../] 
  [./ExecTime]
    type = SPINStats
    quantity='execution_time'
    accumulate_over_time=true
    execute_on = 'timestep_end'
  [../]
[] # Postprocessors

[BCs]
  [./x_bottomleft]
    type = DirichletBC
    variable = disp_x
    boundary = 1
    preset=true
    value = 0
  [../]
  [./y_bottomleft]
    type = DirichletBC
    variable = disp_y
    boundary = 1
    value = 0
    preset=true
  [../]
  [./y_bottomright]
    type = DirichletBC
    variable = disp_y
    boundary = 2
    value = 0
    preset=true
  [../]
  [./ydispfun]
    type = FunctionDirichletBC
    variable = disp_y
    boundary = 3
    function = tfunc
    preset=true
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = PFTransient
  solve_type=Newton

  solver_type = 'ut_SPIN'

  petsc_options_iname = '-snes_spin_additive'  # works on command line, not here
  petsc_options_value = 'true'

  energy_postprocessor = energy_sum
  end_time = 0.4
  dt =1e-3
[]

[MultiApps]
  [./full_solve]
    type = MultiappAssembler
    app_type = PFfracApp
    execute_on = 'timestep_begin'
    positions = '0 0 0 0 0 0'
    input_files = 'sub_disp_r0.i sub_pf_r0.i'
    # clone_master_mesh = true
  [../]
[]

[Transfers]
  [./to_sub]
    type = MultiAppCopyTransfer
    direction = to_multiapp
    source_variable = c
    variable = c
    multi_app = full_solve
    execute_on = 'timestep_end'
  [../]
[]


[Outputs]
  exodus = true
  csv = true
  file_base = "3p_alternate_min"
[]

[Mesh]
  type = FileMesh
  file = /home/kothari/git_repos/moose_pf/pf_frac_spin/meshes/crack_mesh_tension.e
  uniform_refine = 2
  use_displaced_mesh = false
  parallel_type = distributed
[]

[GlobalParams]
  displacements = 'disp_x disp_y'
  alpha_star = 2
  kdamage = 1e-9
  gc = 0.0027
[]

[Problem]
  type = MultiappFEProblem
  solve = false
  kernel_coverage_check = false
[]

[Modules]
  [./TensorMechanics]
    [./Master]
      [./mech]
        add_variables = true
        strain = SMALL
      [../]
    [../]
  [../]
[]



[Variables]
  [./disp_x]
  [../]
  [./disp_y]
  [../]
[]


[AuxVariables]
  [./c]
  [../]
[]


[Functions]
  [./tfunc]
    type = ParsedFunction
    value = '1 * t'
  [../]
[]




[Materials]
  [./pfbulkmat]
    type = CriticalReleaseMaterial
  [../]
  [./elasticity_tensor]
    type = ComputeElasticityTensor
    C_ijkl = '121.15 80.77'
    fill_method = symmetric_isotropic
  [../]
  [./elastic]
    type = ElasticEnergySplit
    c = c
    disp_x = 'disp_x'
    disp_y = 'disp_y'
  [../]     
[]



[BCs]
  [./ydisp]
    type = FunctionDirichletBC
    variable = disp_y
    boundary = 2
    function = tfunc
    preset=true
  [../]
  [./yfix]
    type = DirichletBC
    variable = disp_y
    boundary = 1
    value = 0
    preset=true
  [../]
  [./xfix]
    type = DirichletBC
    variable = disp_x
    boundary = '1 2'
    value = 0
    preset=true
  [../]
[]


[Postprocessors]
  [./elastic_energy]
    execute_on = 'linear'
    energy     = elastic_energy
    type = EnergyIntegral
  [../]   
[] # Postprocessors



[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = PFTransient
  solver_type = 'ut_SPIN'
    solve_type=Newton


  petsc_options_iname = '-snes_spin_additive' 
  petsc_options_value = 'false'



  energy_postprocessor = elastic_energy
  
  # petsc_options_iname = '-snes_max_it -passo_atol -passo_stol -passo_rtol -snes_tr_alg -snes_tr_delta -snes_tr_gamma1 -snes_tr_local_max_iterate -passo_verbose -passo_tr_alg -passo_ksp_atol -snes_atol' 
  # petsc_options_value = '1000 1E-7 1E-7 1E-9 20000000 100000000000 0.2 10 true STEIHAUG_TOINT 1e-15                  1e-12'
  num_steps = 1
[]

[Outputs]
  exodus = false
  csv = false
[]

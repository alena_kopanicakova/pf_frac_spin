create vertex	0			0		0
create vertex	2			0
create vertex	2			2	
create vertex	1.0001			2	
create vertex	1			1.5
create vertex	0.9999			2	
create vertex	0			2


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 1


Create Surface Curve 1 2 3 4 5 6 7

create surface circle radius 0.2
move surface 2 location 0.3 0.3 0 include_merged
subtract body 2 from body 1




nodeset 1 add curve 1
nodeset 2 add curve 2  
nodeset 3 add curve 3
nodeset 4 add curve 6
nodeset 5 add curve 7

surface 3 size 0.025
mesh surface 3



set large exodus file on
export mesh "/Users/alenakopanicakova/code/MOOSE/pf_frac_spin/meshes/antiplane_shear_2d.e" dimension 2 overwrite

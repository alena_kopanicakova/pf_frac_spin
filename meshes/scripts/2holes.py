create vertex	0			0		0
create vertex	2			0
create vertex	2			0.7149	
create vertex	1.9			0.715
create vertex	2			0.7151
create vertex	2			1
create vertex	0 			1
create vertex	0 			0.2851
create vertex	0.1 		0.285
create vertex	0 			0.2849


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 8
create curve 8 9
create curve 9 10
create curve 10 1

Create Surface Curve 1 2 3 4 5 6 7 8 9 10 
sweep surface 1 perpendicular distance 0.1



create surface circle radius 0.2 zplane
move Surface 13 location 0.3 0.7 0.0 include_merged 
sweep surface 13 perpendicular distance 0.1

create surface circle radius 0.2 zplane
move Surface 16 location 1.7 0.3 0.0 include_merged 
sweep surface 16 perpendicular distance 0.1




 subtract body 2 from body 1
 subtract body 3 from body 1


sideset 1 surface 5
sideset 2 surface 10



delete mesh 
volume 1 size auto factor 1
mesh volume 1 


set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/2holes_hex.e" dimension 3 overwrite 




delete mesh 
volume 1 size auto factor 1
mesh volume 1 
HTET volume 1


set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/2holes_htet.e" dimension 3 overwrite 






delete mesh 
volume 1  scheme Tetmesh
volume 1 size auto factor 6
mesh volume 1 



set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/2holes_coarse.e" dimension 3 overwrite 

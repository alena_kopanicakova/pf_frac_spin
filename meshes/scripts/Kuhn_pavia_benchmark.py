create vertex	0			0		0
create vertex	0			2		0
create vertex	2			2		0
create vertex	2			0       0              


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 1

Create Surface Curve 1 2 3 4

sweep surface 1 perpendicular distance 1


create vertex 0.5 0.5 0
create vertex 0.5 1.5 0

split surface 6 across location vertex 13 14

create vertex 1.5 1.5 0
create vertex 1.5 0.5 0

split surface 6 across location vertex 17 18
split surface 6 across location vertex 14 17
split surface 6 across location vertex 13 18


sideset 1 surface 1  
sideset 2 surface 2
sideset 3 surface 3 
sideset 4 surface 4
sideset 5 surface 5
sideset 6 surface 7 
sideset 7 surface 8



volume 1  scheme Tetmesh
volume 1 size auto factor 2
mesh volume 1 



set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/pavia_benchmark_q.e" dimension 3 overwrite 

create vertex	0			0		0
create vertex	0.49		0		0
create vertex	0.5			0.2		0
create vertex	0.51		0		0
create vertex	1			0		0
create vertex	1			0.5		0
create vertex	0			0.5		0


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 1


Create Surface Curve 1 2 3 4 5 6 7
sweep surface 1 perpendicular distance 0.1

create vertex 0.5 0.5 0.1
create vertex 0.5 0.2 0.1

split surface 1 across location vertex 22 23

create vertex 0 0.2 0.1
split surface 11 across location vertex 25 23


create vertex 1 0.2 0.1
split surface 10 across location vertex 27 23



create vertex 0.5 0.5 0
create vertex 0.5 0.2 0
split surface 9 across location vertex 29 30


create vertex 0 0.2 0
split surface 16 across location vertex 30 32


create vertex 1 0.2 0
split surface 17 across location vertex 30 34


sideset 1 surface 12
sideset 2 surface 15
sideset 3 surface 13
sideset 4 surface 14
sideset 5 surface 7
sideset 6 surface 4
sideset 7 surface 8 
sideset 8 surface 3 

sideset 9  surface 20
sideset 10 surface 18
sideset 11 surface 21
sideset 12 surface 19

volume 1  scheme Tetmesh
volume 1 size auto factor 5
mesh volume 1 

set large exodus file on
export mesh "/Users/alenakopanicakova/Desktop/cluster_adapt_download/tension/scaling_c0.e" dimension 3 overwrite 
create vertex	0			0		0
create vertex	0.25		0		0
create vertex	1.75		0		0
create vertex	2			0		0
create vertex	2			0.975	0
create vertex	1.75		0.975	0
create vertex	1.75		1.025	0
create vertex	2			1.025	0
create vertex 	2			2		0
create vertex	0			2		0
create vertex  	0 			1.025	0
create vertex	0.25		1.025	0
create vertex	0.25		0.975	0
create vertex	0			0.975	0



create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 8
create curve 8 9
create curve 9 10
create curve 10 11
create curve 11 12
create curve 12 13
create curve 13 14
create curve 14 1


Create Surface Curve 1 2 3 4 5 6 7 8 9 10 11 12 13 14
sweep surface 1 perpendicular distance 0.5



sideset 1 surface 5
sideset 2 surface 6

sideset 3 surface 7
sideset 4 surface 11

sideset 5 surface 12
sideset 6 surface 13



volume 1 scheme Hexmesh
volume 1 size 0.025
mesh volume 1



set large exodus file on
export mesh "/Users/alenakopanicakova/Desktop/new_meshes_PF/NooruMohamed_05.e" dimension 3 overwrite 



create vertex	0			0		0
create vertex	0.74		0		0
create vertex	0.745 		0.5 	0
create vertex	0.75		0		0
create vertex	1.24		0		0
create vertex	1.245		0.5		0
create vertex	1.25		0		0
create vertex	2			0		0
create vertex	2			1		0
create vertex	0			1		0


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 8
create curve 8 9
create curve 9 10
create curve 10 1

Create Surface Curve 1 2 3 4 5 6 7 8 9 10 
sweep surface 1 perpendicular distance 0.25




sideset 1 surface 7
sideset 2 surface 10

sideset 3 surface 4
sideset 4 surface 3

sideset 5 surface 2
sideset 6 surface 11



volume 1 size auto factor 5
# volume 1  scheme Tetmesh
mesh volume 1 



set large exodus file on
export mesh "/Users/alenakopanicakova/code_base/MOOSE/rook/examples/New_York_WCCM_2018/dynamic/mesh/KW_mesh.e" dimension 3 overwrite 
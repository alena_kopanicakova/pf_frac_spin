create vertex	0		0		0
create vertex	2.50		0		0
create vertex	2.50 	2.50
create vertex	4.70	2.50
create vertex	5.00	2.50
create vertex	5.00 	5.00
create vertex	0 		5.00


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 1

Create Surface Curve 1 2 3 4 5 6 7


sweep surface 1 perpendicular distance 1



sideset 1 surface 7
sideset 2 surface 4
sideset 3 surface 8
sideset 4 surface 6
sideset 5 surface 2
sideset 6 surface 3
sideset 7 surface 5




volume 1 size auto factor 0.01
mesh volume 1




set large exodus file on
export mesh "/Users/alenakopanicakova/Desktop/new_meshes_PF/L_shape3D.e" dimension 3 overwrite 




create vertex	0			0		
create vertex	1			0		
create vertex	3.999		0		
create vertex	4 			1                      
create vertex	4.001 		0
create vertex   19         0       
create vertex	20	 		0
create vertex	20	 		8
create vertex	10	 		8
create vertex	00	 		8


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 8
create curve 8 9
create curve 9 10
create curve 10 1

Create Surface Curve 1 2 3 4 5 6 7 8 9 10

create surface circle radius 0.25 zplane
move Surface 2 location 6 2.75 0.0 include_merged 

create surface circle radius 0.25 zplane
move Surface 3 location 6 4.75 0.0 include_merged

create surface circle radius 0.25 zplane
move Surface 4 location 6 6.75 0.0 include_merged

subtract body 2 from body 1
subtract body 3 from body 1
subtract body 4 from body 1

surface 7  scheme Quadmesh
surface 7 size 0.02
mesh surface 7


nodeset 3 add vertex 9
nodeset 2 add vertex 6
nodeset 1 add vertex 2


sideset 4 add curve 8 9


set large exodus file on
export mesh "/Users/alenakopanicakova/Desktop/new_meshes_PF/bittencourt_beam_2D.e" dimension 2 overwrite 


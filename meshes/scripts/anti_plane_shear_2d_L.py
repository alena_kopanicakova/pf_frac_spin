create vertex	0			0		0
create vertex	2			0
create vertex	2			2
create vertex	1.50001			2
create vertex	1.5			1.5
create vertex	1.49999			2
create vertex	0.50001			2
create vertex	0.5			1.5
create vertex	0.49999			2
create vertex	0			2


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 8
create curve 8 9
create curve 9 10
create curve 10 1


Create Surface Curve 1 2 3 4 5 6 7 8 9 10



nodeset 1 add curve 1
nodeset 2 add curve 2
nodeset 3 add curve 3
nodeset 4 add curve 6
nodeset 5 add curve 9
nodeset 6 add curve 10


surface 1 size 0.025
mesh surface 1



set large exodus file on
export mesh "/Users/alenakopanicakova/code/MOOSE/pf_frac_spin/meshes/antiplane_shear_2d_T.e" dimension 2 overwrite


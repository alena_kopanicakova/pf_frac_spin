create vertex	0			0		0
create vertex	0.39		0		0
create vertex	0.4 		0.1                      
create vertex	0.41 		0
create vertex	2	 		0
create vertex	2	 		0.8
create vertex	0	 		0.8


create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 1

Create Surface Curve 1 2 3 4 5 6 7
sweep surface 1 perpendicular distance 0.2



create surface circle radius 0.025 zplane
move Surface 10 location 0.6 0.275 0.0 include_merged 
sweep surface 10 perpendicular distance 0.2

create surface circle radius 0.025 zplane
move Surface 13 location 0.6 0.475 0.0 include_merged 
sweep surface 13 perpendicular distance 0.2

create surface circle radius 0.025 zplane
move Surface 16 location 0.6 0.675 0.0 include_merged 
sweep surface 16 perpendicular distance 0.2



 subtract body 2 from body 1
 subtract body 3 from body 1
 subtract body 4 from body 1



create vertex 0.1 0 0
create vertex 0.1 0 0.2
create vertex 0.09 0 0
create vertex 0.09 0 0.2
split surface 7 across location vertex 37 36
split surface 29 across location vertex 35 34
sideset 1 surface 30




create vertex 1.9 0 0
create vertex 1.9 0 0.2
create vertex 1.88 0 0
create vertex 1.88 0 0.2
split surface 4 across location vertex 44 45 
split surface 32 across location vertex 43 42  
sideset 2 surface 34



create vertex 1.0 0.8 0
create vertex 1.0 0.8 0.2
create vertex 0.99 0.8 0
create vertex 0.99 0.8 0.2
split surface 2 across location vertex 53 52 
split surface 36 across location vertex 50 51
sideset 3 surface 38



volume 1  scheme Tetmesh
volume 1 size auto factor 1
mesh volume 1 



set large exodus file on
export mesh "/Users/alenakopanicakova/Desktop/new_meshes_PF/bending_holes_2D.e" dimension 3 overwrite 

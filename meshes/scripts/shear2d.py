create vertex	0			0	
create vertex	0.5			0
create vertex	0.5			1
create vertex	0			1
create vertex	0			0.501
create vertex	0.2			0.5
create vertex	0			0.499



create curve 1 2
create curve 2 3
create curve 3 4
create curve 4 5
create curve 5 6
create curve 6 7
create curve 7 1


Create Surface Curve 1 2 3 4 5 6 7



sideset 1 curve 1
sideset 2 curve 3



volume 1 size auto factor 1
mesh volume 1 


set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/2holes_hex.e" dimension 3 overwrite 




delete mesh 
volume 1 size auto factor 1
mesh volume 1 
HTET volume 1


set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/2holes_htet.e" dimension 3 overwrite 






delete mesh 
volume 1  scheme Tetmesh
volume 1 size auto factor 6
mesh volume 1 



set large exodus file on
export mesh "/Users/alenakopanicakova/MOOSE/rook/2holes_coarse.e" dimension 3 overwrite 

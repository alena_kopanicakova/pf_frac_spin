#pragma once

#include "Kernel.h"

// Forward Declarations
// class RankTwoTensor;
class PFDrivingForce;

template <> InputParameters validParams<PFDrivingForce>();

class PFDrivingForce : public Kernel {
public:
  PFDrivingForce(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

  /// Contribution of umdamaged strain energy to damage evolution
  const MaterialProperty<Real> &_G0_pos;

  Real _kdamage;
  Real _alpha;
  Real _beta;
  Real _gamma;
  Real _delta;
  Real _alpha_star;

  /// Variation of undamaged strain energy driving damage evolution with strain
  const MaterialProperty<RankTwoTensor> *_dG0_pos_dstrain;

  /// Coupled displacement variables
  const unsigned int _ndisp;
  std::vector<unsigned int> _disp_var;
  std::string _base_name;
};

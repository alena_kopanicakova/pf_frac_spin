#pragma once

#include "Reaction.h"

// Forward Declarations
class PFReaction;

template <> InputParameters validParams<PFReaction>();

class PFReaction : public Reaction {
public:
  PFReaction(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual() override;
  virtual Real computeQpJacobian() override;

  const MaterialProperty<Real> &_gc_prop;
  Real _l;
};

#pragma once

#include "Function.h"
#include "Kernel.h"

// Forward Declarations
class PFDiffusion;

template <> InputParameters validParams<PFDiffusion>();

class PFDiffusion : public Kernel {
public:
  PFDiffusion(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:
  const MaterialProperty<Real> &_gc_prop;
  Real _l;
};

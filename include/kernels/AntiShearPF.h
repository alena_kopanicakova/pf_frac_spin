#pragma once

#include "Function.h"
#include "Kernel.h"

// Forward Declarations
class AntiShearPF;

template <> InputParameters validParams<AntiShearPF>();

class AntiShearPF : public Kernel {
public:
  AntiShearPF(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const MaterialProperty<Real> &_gc_prop;
  Real _l;

  Real _alpha;
  Real _beta;
  Real _gamma;
  Real _delta;
  Real _alpha_star;

  Real _mu;

  const VariableGradient &_disp_grad;
};

#pragma once

#include "Function.h"
#include "Kernel.h"

// Forward Declarations
class AntiShearPFAniso;

template <> InputParameters validParams<AntiShearPFAniso>();

class AntiShearPFAniso : public Kernel {
public:
  AntiShearPFAniso(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const MaterialProperty<Real> &_gc_prop;
  Real _l;

  Real _alpha;
  Real _beta;
  Real _gamma;
  Real _delta;
  Real _alpha_star;

  Real _mu;
  Real _omega;
  Real _tau;

  RankTwoTensor _B;

  const VariableGradient &_disp_grad;
};

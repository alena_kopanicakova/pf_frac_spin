#pragma once

#include "Function.h"
#include "Kernel.h"

// Forward Declarations
class PFSubproblem;

template <> InputParameters validParams<PFSubproblem>();

class PFSubproblem : public Kernel {
public:
  PFSubproblem(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const MaterialProperty<Real> &_gc_prop;
  Real _l;

  /// Contribution of umdamaged strain energy to damage evolution
  const MaterialProperty<Real> &_G0_pos;

  // Real _kdamage;
  Real _alpha;
  Real _beta;
  Real _gamma;
  Real _delta;
  Real _alpha_star;

  /// Variation of undamaged strain energy driving damage evolution with strain
  const MaterialProperty<RankTwoTensor> *_dG0_pos_dstrain;

  /// Coupled displacement variables
  const unsigned int _ndisp;
  std::vector<unsigned int> _disp_var;
  std::string _base_name;

  bool _use_off_diag_terms;
};

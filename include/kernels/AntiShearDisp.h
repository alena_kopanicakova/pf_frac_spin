#pragma once

#include "Function.h"
#include "Kernel.h"

// Forward Declarations
class AntiShearDisp;

template <> InputParameters validParams<AntiShearDisp>();

class AntiShearDisp : public Kernel {
public:
  AntiShearDisp(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();
  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:
  const VariableValue &_c;

  // Real _kdamage;
  Real _alpha;
  Real _beta;
  Real _gamma;
  Real _delta;
  Real _alpha_star;

  Real _mu;
};

#pragma once

#include "Function.h"
#include "Kernel.h"
// Forward Declarations
class PFIrreversibilityPenalty;
template <> InputParameters validParams<PFIrreversibilityPenalty>();
class PFIrreversibilityPenalty : public Kernel {
public:
  PFIrreversibilityPenalty(const InputParameters &parameters);

protected:
  virtual Real computeQpResidual();
  virtual Real computeQpJacobian();

private:
  Real _alpha;
  const VariableValue &_u_old;
};

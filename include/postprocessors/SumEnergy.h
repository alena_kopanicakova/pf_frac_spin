#pragma once

#include "GeneralPostprocessor.h"

class EnergySum;
template <> InputParameters validParams<EnergySum>();

class EnergySum : public GeneralPostprocessor

{

public:
  EnergySum(const InputParameters &parameters);

  virtual ~EnergySum();
  virtual void initialize();

  virtual void execute();

  virtual Real getValue();

protected:
  std::vector<PostprocessorName> _energy_names;
  std::vector<std::reference_wrapper<const PostprocessorValue>> _energies;

  Real _value;
};

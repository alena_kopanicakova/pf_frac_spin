#pragma once

#include "ElementIntegralPostprocessor.h"

// Forward Declarations
class FracIrrEnergyIntegral;

template <> InputParameters validParams<FracIrrEnergyIntegral>();

/**
 * Postrocessor to calculate energy
 * One needs to provide name to material property
 */
class FracIrrEnergyIntegral : public ElementIntegralPostprocessor {
public:
  FracIrrEnergyIntegral(const InputParameters &parameters);

protected:
  virtual Real computeQpIntegral();

  MaterialProperty<Real> const &_energy_frac;
  MaterialProperty<Real> const &_energy_irr;
};

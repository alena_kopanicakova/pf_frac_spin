#pragma once

#include "GeneralPostprocessor.h"

class SPINStats;

template <> InputParameters validParams<SPINStats>();

class SPINStats : public GeneralPostprocessor {
public:
  static InputParameters validParams();

  SPINStats(const InputParameters &parameters);

  virtual void timestepSetup() override;

  virtual void initialize() override {}
  virtual void execute() override {}

  virtual Real getValue() override;

protected:
  FEProblemBase *_fe_problem;

  bool _accumulate_over_time;

  Real _num_iters;

  Real _time;

  std::string _quantity;
};

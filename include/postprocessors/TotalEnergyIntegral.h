#pragma once

#include "ElementIntegralPostprocessor.h"

// Forward Declarations
class TotalEnergyIntegral;

template <> InputParameters validParams<TotalEnergyIntegral>();

/**
 * Postrocessor to calculate energy
 * One needs to provide name to material property
 */
class TotalEnergyIntegral : public ElementIntegralPostprocessor {
public:
  TotalEnergyIntegral(const InputParameters &parameters);

protected:
  virtual Real computeQpIntegral();

  MaterialProperty<Real> const &_energy_frac;
  MaterialProperty<Real> const &_energy_irr;
  MaterialProperty<Real> const &_energy_elastic;
};

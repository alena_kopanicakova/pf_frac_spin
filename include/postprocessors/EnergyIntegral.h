#pragma once

#include "ElementIntegralPostprocessor.h"

// Forward Declarations
class EnergyIntegral;

template <> InputParameters validParams<EnergyIntegral>();

/**
 * Postrocessor to calculate energy
 * One needs to provide name to material property
 */
class EnergyIntegral : public ElementIntegralPostprocessor {
public:
  EnergyIntegral(const InputParameters &parameters);

protected:
  virtual Real computeQpIntegral();

  MaterialProperty<Real> const &_energy;
};

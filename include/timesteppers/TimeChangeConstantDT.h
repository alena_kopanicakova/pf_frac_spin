#pragma once

#include "TimeStepper.h"

class TimeChangeConstantDT;

template <> InputParameters validParams<TimeChangeConstantDT>();

class TimeChangeConstantDT : public TimeStepper {
public:
  static InputParameters validParams();

  TimeChangeConstantDT(const InputParameters &parameters);

protected:
  virtual Real computeInitialDT() override;
  virtual Real computeDT() override;

private:
  const Real _constant_dt;
  const Real _growth_factor;
  const Real _dt_after_switch;
  const Real _switch_time;
};

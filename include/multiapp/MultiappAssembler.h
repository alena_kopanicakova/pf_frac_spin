#pragma once

#include "AuxiliarySystem.h"
#include "Executioner.h"
#include "FEProblem.h"
#include "MooseVariable.h"
#include "MultiApp.h"
#include "NonlinearSystem.h"
#include "libmesh/nonlinear_implicit_system.h"
#include "libmesh/petsc_vector.h"
#include <petsc/private/snesimpl.h>
#include <petscsnes.h>
#include <utopia.hpp>

// Forward declarations
class MultiappAssembler;
class Executioner;
template <> InputParameters validParams<MultiappAssembler>();
class MultiappAssembler : public MultiApp {
public:
  MultiappAssembler(const InputParameters &parameters);
  ~MultiappAssembler();
  virtual void initialSetup() override;
  virtual bool solveStep(Real dt, Real target_time,
                         bool auto_advance = true) override;

  // virtual void advanceStep() override{};
  virtual bool collect_snes();
  // virtual bool mark_DirichletBC();

  // virtual bool increase_dt(Real dt);
  virtual bool set_up_initial_guesses();

  virtual LocalRankConfig buildComm(bool batch_mode) override;

  virtual bool make_assembler_ready(SNES &snes);
  virtual bool pre_process(const Real &global_time, const int &step_num = 1,
                           const Real &global_dt = 1);
  virtual bool post_process();

  virtual void transfer_within_mesh(FEProblemBase &from_problem,
                                    FEProblemBase &to_problem,
                                    const VariableName &_from_var_name,
                                    const VariableName &_to_var_name);

  virtual void transferDofObject(libMesh::DofObject *to_object,
                                 libMesh::DofObject *from_object,
                                 MooseVariableFEBase &to_var,
                                 MooseVariableFEBase &from_var);

  template <class Vector>
  void get_disp_to_mono_transfer(
      const typename utopia::Traits<Vector>::SizeType num_vars_mono,
      std::function<void(const Vector &, Vector &)> &disp_to_mono) {

    using namespace utopia;
    using SizeType = typename utopia::Traits<Vector>::SizeType;

    disp_to_mono = [num_vars_mono](const Vector &disp, Vector &mono) {
      {

        Read<Vector> r(disp);
        Write<Vector> w(mono);
        Range range_w = range(disp);
        for (SizeType disp_id = range_w.begin(); disp_id != range_w.end();
             disp_id++) {
          SizeType node_id = SizeType(disp_id / (num_vars_mono - 1));
          SizeType reminder = disp_id % (num_vars_mono - 1);
          SizeType dof_id_mono = (node_id * num_vars_mono) + reminder;
          mono.set(dof_id_mono, disp.get(disp_id));
        }
      }
    };
  };

  template <class Vector>
  void get_pf_to_mono_transfer(
      const int num_vars_mono,
      std::function<void(const Vector &, Vector &)> &pf_to_mono) {

    using namespace utopia;
    using SizeType = typename utopia::Traits<Vector>::SizeType;

    pf_to_mono = [num_vars_mono](const Vector &pf, Vector &mono) {
      {

        Read<utopia::PetscVector> r(pf);
        Write<utopia::PetscVector> w(mono);
        Range range_w = range(pf);
        for (SizeType pf_id = range_w.begin(); pf_id != range_w.end();
             pf_id++) {

          PetscInt dof_id_mono = (pf_id * num_vars_mono) + (num_vars_mono - 1);
          mono.set(dof_id_mono, pf.get(pf_id));
        }
      }
    };
  };

  // names need to be same!
  template <class Vector>
  void get_mono_to_disp_transfer(
      const typename utopia::Traits<Vector>::SizeType num_vars_mono,
      std::function<void(const Vector &, Vector &)> &mono_to_disp) {

    using namespace utopia;
    using SizeType = typename utopia::Traits<Vector>::SizeType;

    mono_to_disp = [this, num_vars_mono](const Vector &mono, Vector &disp) {
      {

        // update monolithic system
        libMesh::PetscVector<libMesh::Number> &sol =
            *libmesh_cast_ptr<libMesh::PetscVector<libMesh::Number> *>(
                &this->_fe_problem.getNonlinearSystemBase().solution());

        Vec sol_petsc = sol.vec();
        utopia::convert(mono, sol_petsc);

        const std::vector<VariableName> aux_var_names_disp =
            this->_apps[0]
                ->getExecutioner()
                ->feProblem()
                .getAuxiliarySystem()
                .getVariableNames();

        for (auto v = 0; v < aux_var_names_disp.size(); v++) {
          this->transfer_within_mesh(
              this->_fe_problem, this->_apps[0]->getExecutioner()->feProblem(),
              aux_var_names_disp[v], aux_var_names_disp[v]);
        }

        {
          Read<Vector> r(mono);
          Write<Vector> w(disp);
          Range range_w = range(disp);
          for (int disp_id = range_w.begin(); disp_id != range_w.end();
               disp_id++) {
            SizeType node_id = SizeType(disp_id / (num_vars_mono - 1));
            SizeType reminder = disp_id % (num_vars_mono - 1);
            SizeType dof_id_mono = (node_id * num_vars_mono) + reminder;
            disp.set(disp_id, mono.get(dof_id_mono));
          }
        }
      }
    };
  };

  template <class Vector>
  void get_mono_to_pf_transfer(
      const typename utopia::Traits<Vector>::SizeType num_vars_mono,
      std::function<void(const Vector &, Vector &)> &pf_to_mono) {

    using namespace utopia;
    using SizeType = typename utopia::Traits<Vector>::SizeType;

    pf_to_mono = [this, num_vars_mono](const Vector &mono, Vector &pf) {
      {

        // update monolithic system
        libMesh::PetscVector<libMesh::Number> &sol =
            *libmesh_cast_ptr<libMesh::PetscVector<libMesh::Number> *>(
                &this->_fe_problem.getNonlinearSystemBase().solution());

        Vec sol_petsc = sol.vec();
        utopia::convert(mono, sol_petsc);

        const std::vector<VariableName> aux_var_names_pf =
            this->_apps[1]
                ->getExecutioner()
                ->feProblem()
                .getAuxiliarySystem()
                .getVariableNames();

        for (auto v = 0; v < aux_var_names_pf.size(); v++) {
          this->transfer_within_mesh(
              this->_fe_problem, this->_apps[1]->getExecutioner()->feProblem(),
              aux_var_names_pf[v], aux_var_names_pf[v]);
        }

        {
          Read<utopia::PetscVector> r(mono);
          Write<utopia::PetscVector> w(pf);
          Range range_w = range(pf);
          for (SizeType pf_id = range_w.begin(); pf_id != range_w.end();
               pf_id++) {

            PetscInt dof_id_mono =
                (pf_id * num_vars_mono) + (num_vars_mono - 1);
            pf.set(pf_id, mono.get(dof_id_mono));
          }
        }
      }
    };
  };

  template <class Vector>
  void
  get_transfers(const typename utopia::Traits<Vector>::SizeType num_vars_mono,
                std::function<void(const Vector &, Vector &)> &disp_to_mono,
                std::function<void(const Vector &, Vector &)> &pf_to_mono,
                std::function<void(const Vector &, Vector &)> &mono_to_disp,
                std::function<void(const Vector &, Vector &)> &mono_to_pf) {

    get_disp_to_mono_transfer(num_vars_mono, disp_to_mono);
    get_pf_to_mono_transfer(num_vars_mono, pf_to_mono);
    get_mono_to_disp_transfer(num_vars_mono, mono_to_disp);
    get_mono_to_pf_transfer(num_vars_mono, mono_to_pf);
  }

protected:
  std::vector<Executioner *> _executioners;
  std::vector<SNES> _sub_sness;
  // std::vector<Vec> _init_guesses;
  // std::vector<Vec> _dbc_markers;
  Vec _init_guess_fine;
  std::string _solver_type;
};

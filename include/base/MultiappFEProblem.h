#pragma once

#include "FEProblem.h"

class MultiappFEProblem;
template <> InputParameters validParams<MultiappFEProblem>();
class MultiappFEProblem : public FEProblem {
public:
  MultiappFEProblem(const InputParameters &parameters);
  virtual ~MultiappFEProblem();
  virtual void solve() override;

private:
  const std::string _multilevel_multiapp;
  bool _auto_dt;
};

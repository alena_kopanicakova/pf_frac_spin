#pragma once

#include "Executioner.h"
#include "GenericSNESSolver.h"
#include "Transient.h"
#include <petscsnes.h>

// System includes
#include <fstream>
#include <string>

// Forward Declarations
class PFTransient;
class TimeStepper;
class FEProblemBase;
class Transient;

PETSC_EXTERN PetscErrorCode FormObjectiveFunctionTransient(SNES snes, Vec x,
                                                           PetscReal *energy,
                                                           void *ctx);

template <> InputParameters validParams<PFTransient>();

/**
 * Transient executioners usually loop through a number of timesteps... calling
 * solve() for each timestep.
 */
class PFTransient : public Transient {
public:
  static InputParameters validParams();

  PFTransient(const InputParameters &parameters);
  virtual void get_postprocessorvalue(Real &result);

  void set_current_step(const int &step_num) { _t_step = step_num; }
  int get_current_step() { return _t_step; }

  void set_dt(const Real &dt) { _dt = dt; }
  Real get_dt() { return _dt; }

  FEProblemBase &problem() { return this->_problem; }

  virtual void takeStep(Real input_dt) override;

  void setupSolver();

  GenericSNESSolver *_solver_ptr;

protected:
  std::string _solver_type;
};
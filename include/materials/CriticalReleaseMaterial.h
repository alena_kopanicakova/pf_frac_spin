#pragma once

#include "Function.h"
#include "Material.h"

class CriticalReleaseMaterial;

template <> InputParameters validParams<CriticalReleaseMaterial>();

class CriticalReleaseMaterial : public Material {
public:
  CriticalReleaseMaterial(const InputParameters &parameters);

protected:
  virtual void initQpStatefulProperties();
  virtual void computeQpProperties();
  /**
   * This function obtains the value of gc
   * Must be overidden by the user for heterogeneous gc
   */
  virtual void getProp();

  /// Input parameter for homogeneous gc
  Real _gc;

  /// Material property where the gc values are stored
  MaterialProperty<Real> &_gc_prop;
  /// Function to specify varying gc
  const Function *_function_prop;

private:
};

#pragma once

#include "ComputeStressBase.h"
#include "Function.h"

class IrreversibilityPenaltyEnergy : public Material {
public:
  IrreversibilityPenaltyEnergy(const InputParameters &parameters);

protected:
  virtual void initQpStatefulProperties() override;
  virtual void computeQpProperties() override;

  const VariableValue &_c;
  const VariableValue &_c_old;

  MaterialProperty<Real> &_irr_energy;
  const Real _alpha;
};

#pragma once

#include "ComputeStressBase.h"
#include "Function.h"

class FracEnergy : public Material {
public:
  FracEnergy(const InputParameters &parameters);

protected:
  virtual void initQpStatefulProperties() override;
  virtual void computeQpProperties() override;

  const VariableValue &_c;
  const VariableGradient &_grad_c;

  MaterialProperty<Real> &_PF_energy;
  const Real _gc;
  const Real _l;
};

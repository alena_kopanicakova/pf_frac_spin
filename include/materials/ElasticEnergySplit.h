#pragma once

#include "ComputeStressBase.h"
#include "Function.h"

/**
 * Phase-field fracture
 * This class computes the energy contribution to damage growth
 * Small strain Isotropic Elastic formulation
 * Stiffness matrix scaled for heterogeneous elasticity property
 */
class ElasticEnergySplit : public ComputeStressBase {
public:
  ElasticEnergySplit(const InputParameters &parameters);

protected:
  virtual void computeQpStress();
  virtual void updateVar();
  // virtual void updateJacobian();

  virtual void initQpStatefulProperties();

  const VariableValue &_c;

  /// Name of the elasticity tensor material property
  const std::string _elasticity_tensor_name;
  /// Elasticity tensor material property
  const MaterialProperty<RankFourTensor> &_elasticity_tensor;

  // Small number to avoid non-positive definiteness at or near complete damage
  Real _kdamage;

  // params for degradation function
  Real _alpha;
  Real _beta;
  Real _gamma;
  Real _delta;
  Real _alpha_star;

  MaterialProperty<Real> &_G0_pos;
  MaterialProperty<Real> &_G0_neg;

  MaterialProperty<Real> &_elastic_energy;
  MaterialProperty<Real> &_positive_energy_deg;
  MaterialProperty<Real> &_negative_energy;

  const MaterialProperty<Real> &_G0_pos_history;

  MaterialProperty<RankTwoTensor> &_dstress_dc;
  MaterialProperty<RankTwoTensor> &_dG0_pos_dstrain;

  std::vector<RankTwoTensor> _etens;

  std::vector<Real> _epos;
  std::vector<Real> _eneg;

  std::vector<Real> _eigval;
  RankTwoTensor _eigvec;

  MaterialProperty<Real> &_pressure;
  const Function *_function_presure;

  bool _stagg_scheme;
  bool _coarse_level;

  const VariableGradient &_grad_disp_x;
  const VariableGradient &_grad_disp_y;
  const VariableGradient &_grad_disp_z;

  MaterialProperty<RealTensorValue> &_U;
  MaterialProperty<RealTensorValue> &_F;
  MaterialProperty<Real> &_J;

  RealTensorValue _Id;
};

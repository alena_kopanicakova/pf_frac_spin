#ifndef PETSC_UTOPIA_FINCTION_HPP
#define PETSC_UTOPIA_FINCTION_HPP

#include "utopia_Core.hpp"
#include "utopia_ExtendedFunction.hpp"

#include "utopia_petsc_Types.hpp"

#include "PFfracApp.h"
#include <petsc/private/snesimpl.h>
#include <petscsnes.h>

namespace utopia {
template <class Matrix, class Vector, int Backend = Traits<Matrix>::Backend>
class PETSCSnesUtopiaInterface {};

template <class Matrix, class Vector>
class PETSCSnesUtopiaInterface<Matrix, Vector, PETSC>
    : public ExtendedFunction<Matrix, Vector> {
  using Scalar = typename utopia::Traits<Vector>::Scalar;

public:
  // PETSCUtopiaNonlinearFunction(SNES snes, const Vector & x_init =
  // local_zeros(1), const Vector & bc_marker = local_zeros(1), const Vector &
  // rhs = local_zeros(1)) :
  PETSCSnesUtopiaInterface(SNES snes, const Vector &x_init = Vector(),
                           const Vector &bc_marker = Vector())
      : ExtendedFunction<Matrix, Vector>(x_init, bc_marker), snes_(snes) {}

  bool gradient(const Vector &x, Vector &g) const override {
    // initialization of gradient vector...
    if (empty(g)) {
      g.zeros(layout(x));
    }

    SNESComputeFunction(snes_, raw_type(x), raw_type(g));

    return true;
  }

  bool hessian(const Vector &x, Matrix &hessian) const override {
    SNESComputeJacobian(snes_, raw_type(x), snes_->jacobian,
                        snes_->jacobian_pre);
    wrap(snes_->jacobian, hessian);
    return true;
  }

  bool value(const Vector &x, typename Vector::Scalar &result) const override {
    // hack to have fresh energy (MOOSE post-processor does things in strange
    // way )
    Vector grad = 0 * x;
    this->gradient(x, grad);

    DM dm;
    DMSNES sdm;

    SNESGetDM(snes_, &dm);
    DMGetDMSNES(dm, &sdm);
    if (sdm->ops->computeobjective) {
      SNESComputeObjective(snes_, raw_type(x), &result);
    } else {
      // Vector grad = 0 * x;
      // this->gradient(x, grad);
      result = 0.5 * norm2(grad) * norm2(grad);
    }

    // std::cout << "--- Yes, here... \n";

    return true;
  }

  bool value_grad(const Vector &x, typename Vector::Scalar &result,
                  Vector &grad) const override {
    if (empty(grad)) {
      grad.zeros(layout(x));
    }

    this->gradient(x, grad);

    DM dm;
    DMSNES sdm;

    SNESGetDM(snes_, &dm);
    DMGetDMSNES(dm, &sdm);
    if (sdm->ops->computeobjective) {
      SNESComputeObjective(snes_, raw_type(x), &result);
    } else {
      // Vector grad = 0 * x;
      // this->gradient(x, grad);
      result = 0.5 * norm2(grad) * norm2(grad);
    }

    // std::cout << "--- Yes, here... \n";

    return true;
  }

  virtual void getSNES(SNES &snes) { snes = snes_; }

private:
  SNES snes_;
  Vector grad_;
};

} // namespace utopia

#endif // PETSC_UTOPIA_FINCTION_HPP

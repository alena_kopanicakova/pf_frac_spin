#pragma once

#include "GenericSNESSolver.h"
#include "utopia_CubicBacktracking.h"
#include "utopia_Newton_SPIN.h"
#include "utopia_TwoFieldSPIN.h"
#include <petscsnes.h>
#include <utopia.hpp>

PETSC_EXTERN PetscErrorCode SNESCreate_SPIN_Solver(SNES snes);

struct SNES_SPIN : public GenericSNESSolver {

  PetscBool additive_flg = PETSC_FALSE;
  PetscBool use_switch = PETSC_FALSE;
  PetscScalar snes_spin_action_rtol = 1e-6;

  PetscScalar snes_spin_disp_diff_tol = 1e-15;
  PetscScalar snes_spin_c_diff_tol = 1e-5;

  PetscBool diag_scale_linear_systems = PETSC_FALSE;

  PetscBool snes_spin_use_exact_hessian = PETSC_FALSE;
  PetscBool snes_spin_use_block_precond_global = PETSC_FALSE;
};

#ifndef UTOPIA_SOLVER_NEWTON_SPIN_HPP
#define UTOPIA_SOLVER_NEWTON_SPIN_HPP

#include "utopia_Core.hpp"
#include "utopia_Function.hpp"
#include "utopia_LS_Strategy.hpp"
#include "utopia_LinearSolver.hpp"
#include "utopia_NewtonBase.hpp"
#include "utopia_NonLinearSolver.hpp"

#include <iomanip>
#include <limits>

namespace utopia {

template <class Matrix, class Vector, int Backend = Traits<Vector>::Backend>
class Newton_SPIN final : public NewtonBase<Matrix, Vector> {
  using Scalar = typename Traits<Vector>::Scalar;
  using SizeType = typename Traits<Vector>::SizeType;
  using Layout = typename Traits<Vector>::Layout;

  typedef typename NewtonBase<Matrix, Vector>::Solver Solver;
  using LSStrategy = utopia::LSStrategy<Vector>;

public:
  Newton_SPIN(const std::shared_ptr<Solver> &linear_solver =
                  std::make_shared<ConjugateGradient<Matrix, Vector>>())
      : NewtonBase<Matrix, Vector>(linear_solver), alpha_(1.0) {}

  bool solve(Function<Matrix, Vector> &fun, Vector &x) override {
    using namespace utopia;

    if (empty(x)) {
      utopia_error("utopia::Newton_SPIN, initial guess is empty vector");
    }

    init_memory(layout(x));

    Scalar g_norm = 1, g0_norm = 1, r_norm = 1, s_norm = 1, J;
    SizeType it = 0;

    bool converged = false;

    // notify listener
    fun.project_onto_feasibile_region(x);
    fun.update(x);

    fun.gradient(x, grad_);
    g0_norm = norm2(grad_);
    g_norm = g0_norm;

    this->init_solver("NEWTON",
                      {" it. ", "|| g ||", "r_norm", "|| p_k || ", "alpha_k"});

    if (this->verbose_)
      PrintInfo::print_iter_status(it, {g_norm, J});
    it++;

    while (!converged) {
      // find direction step
      step_.set(0.0);

      // setting up adaptive stopping criterium for linear solver
      if (this->has_forcing_strategy()) {
        if (auto *iterative_solver =
                dynamic_cast<IterativeSolver<Matrix, Vector> *>(
                    this->linear_solver_.get())) {
          iterative_solver->atol(this->estimate_ls_atol(g_norm, it));
        } else {
          utopia_error(
              "utopia::Newton::you can not use inexact Newton with exact "
              "linear solver. ");
        }
      }

      if (this->has_preconditioned_solver() && fun.has_preconditioner()) {
        fun.hessian(x, hessian_, preconditioner_);
        grad_neg_ = -1.0 * grad_;

        if (!this->check_values(it, fun, x, grad_, hessian_))
          return false;

        this->linear_solve(hessian_, preconditioner_, grad_neg_, step_);
      } else {
        fun.hessian(x, hessian_);
        grad_neg_ = -1.0 * grad_;

        if (!this->check_values(it, fun, x, grad_, hessian_))
          return false;

        this->linear_solve(hessian_, grad_neg_, step_);
      }

      // update x
      if (fabs(alpha_ - 1) < std::numeric_limits<Scalar>::epsilon()) {
        x += step_;
      } else {
        x += alpha_ * step_;
      }

      // notify listener
      fun.update(x);
      fun.gradient(x, grad_);

      // norms needed for convergence check
      norms2(grad_, step_, g_norm, s_norm);
      r_norm = g_norm / g0_norm;

      // // print iteration status on every iteration
      if (this->verbose_)
        PrintInfo::print_iter_status(it, {g_norm, r_norm, s_norm, alpha_});

      // // check convergence and print interation info
      converged = this->check_convergence(it, g_norm, r_norm, s_norm);
      it++;
    }

    this->print_statistics(it);

    return true;
  }

  void read(Input &in) override {
    NewtonBase<Matrix, Vector>::read(in);
    in.get("dumping", alpha_);
  }

  void print_usage(std::ostream &os) const override {
    NewtonBase<Matrix, Vector>::print_usage(os);

    this->print_param_usage(os, "dumping", "real", "Default step size.", "1.0");
    this->print_param_usage(os, "line-search", "LSStrategy",
                            "Input parameters for line-search strategy.", "-");
  }

  void init_memory(const Layout &layout) {
    // init of linear solver
    NewtonBase<Matrix, Vector>::init_memory(layout);

    // init of vectors
    grad_neg_.zeros(layout);
    step_.zeros(layout);
    grad_.zeros(layout);
    // D_inv_.zeros(layout);
  }

private:
  Scalar alpha_; /*!< Dumping parameter. */
  Vector grad_neg_, step_, grad_;
  Matrix D_inv_;

public:
  Matrix hessian_, preconditioner_;
};

} // namespace utopia
#endif // UTOPIA_SOLVER_NEWTON_SPIN_HPP

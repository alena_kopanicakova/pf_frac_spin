#pragma once

#include "GenericSNESSolver.h"
#include "utopia_TwoFieldAlternateMinimization.h"
#include <petscsnes.h>
#include <utopia.hpp>

PETSC_EXTERN PetscErrorCode SNESCreate_AlternateMinimization_Solver(SNES snes);

struct SNES_AlternateMinimization : public GenericSNESSolver {

  PetscBool exact_solve = PETSC_TRUE;
  PetscBool direct_solver = PETSC_FALSE;

  PetscScalar snes_am_disp_diff_tol = 1e-15;
  PetscScalar snes_am_c_diff_tol = 1e-5;
};

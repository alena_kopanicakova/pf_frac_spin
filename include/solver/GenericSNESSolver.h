#pragma once

#include <iostream>
#include <petscsnes.h>
#include <utopia.hpp>
#include <vector>

struct GenericSNESSolver {

  std::vector<SNES> sub_sness;
  std::vector<Vec> init_guesses;
  std::vector<Vec> bc_dirichlet_marker;

  PetscInt num_variables = 0;
  PetscBool initialized = PETSC_FALSE; /* flag for convergence testing */

  std::function<void(const utopia::PetscVector &, utopia::PetscVector &)>
      disp_to_mono;
  std::function<void(const utopia::PetscVector &, utopia::PetscVector &)>
      pf_to_mono;
  std::function<void(const utopia::PetscVector &, utopia::PetscVector &)>
      mono_to_disp;
  std::function<void(const utopia::PetscVector &, utopia::PetscVector &)>
      mono_to_pf;

  PetscInt nl_iterates_global = 0;
  PetscInt nl_iterates_field1 = 0;
  PetscInt nl_iterates_field2 = 0;

  PetscInt num_linear_solves_global = 0;
  PetscInt num_linear_solves_field1 = 0;
  PetscInt num_linear_solves_field2 = 0;

  PetscInt sum_linear_its_global = 0;
  PetscInt sum_linear_its_field1 = 0;
  PetscInt sum_linear_its_field2 = 0;

  PetscReal execution_time = 0.0;

  PetscScalar cond_number_precond = 1.0;
  PetscScalar cond_number_unprecond = 1.0;
};

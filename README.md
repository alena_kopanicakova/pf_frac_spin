# SPIN preconditioners for phase-field fracture simulations based on the finite element framework MOOSE
This repository contains the code used to generate results in the paper: <br> 
**Nonlinear Field-split Preconditioners for Solving Monolithic Phase-field Models of Brittle Fracture by Kopanicakova, Kothari, Krause.**<br> 

If you use the developed code/its components for your research, please use the following bibtex entry (or equivalent) to cite us
```bibtex
@article{kopanicakova_kothari_krause_22,
title = {Nonlinear field-split preconditioners for solving monolithic phase-field models of brittle fracture},
journal = {Computer Methods in Applied Mechanics and Engineering},
volume = {403},
pages = {115733},
year = {2023},
issn = {0045-7825},
doi = {https://doi.org/10.1016/j.cma.2022.115733},
url = {https://www.sciencedirect.com/science/article/pii/S0045782522006880},
author = {Alena Kopani{\v{c}}{\'a}kov{\'a} and Hardik Kothari and Rolf Krause},
}
```

### Examples
Input files used to obtain numerical results in the paper can be found in folder **examples_spin**

#### Running example
You can run example, as follows (You can run code on HPC cluster by adapting provided bash/slurm scripts):<br> 
- cd examples_spin/01_tension2D<br> 
- mpirun -n 10  ../../pf_frac-opt -i master_dt_5e-5_r0.i  -snes_spin_additive false -snes_atol 1e-6 -snes_rtol 1e-7 -snes_stol 1e-9 -snes_max_it 300 <br> 

#### Selection of solver
For all numerical examples, the particular choice of solver can be specified by prescribing **solver_type** option in Executioner block of the input file. 
In particular, we support

|     solver_type           |            description            |
|---------------------------|-----------------------------------|
| ut_alternate_minimization | Use alternate minimization solver |
| ut_SPIN     				| Use SPIN solver                   |


#### Command line arguments
Both solvers, alternate minimization and SPIN can be configured on command line. <br>
Set of supported PETSc options for alternate minimization: 


|     option                |            values     |  description            |
|---------------------------|-----------------------|----------------------	  |
| -snes_atol 				| 1e-7 					| Absolute tolerance on norm of gradient of coupled residual.|
| -snes_rtol     			| 1e-6                  | Relative tolerance on norm of gradient of coupled residual.|
| -snes_max_it     			| 50000                 | Maximum number of iterations.|
| -snes_am_c_diff_tol     	| 1e-4                  | Tolerance on the change in the phase-field (measured in the inf. norm).|
| -snes_am_disp_diff_tol     	| 1e-12                  | Tolerance on the change in the displacement (measured in the inf. norm).|
| -snes_am_exact_solve     	| True                  | Flag specifing whether arrising linear systems exactly or no (use inexact Newton's method with adaptive stopping crit.). Flag is always true, if direct solver is used.|
| -snes_am_direct_solver    | False                 | Flag specifing whether arrising linear systems are solved using direct solver (LU/Mumps) or iterative method (precond. CG).|



<br>Set of supported PETSc options for SPIN solver: 

|     option                |            values     |  description            |
|---------------------------|-----------------------|----------------------	  |
| -snes_atol 				| 1e-7 					| Absolute tolerance on norm of gradient of coupled residual.|
| -snes_rtol     			| 1e-6                  | Relative tolerance on norm of gradient of coupled residual.|
| -snes_stol     			| 1e-8                  | Absolute tolerance on norm of correction.|
| -snes_max_it     			| 50000                 | Maximum number of iterations|
| -snes_spin_additive     	| true                  | Flag specifying whether additive or multiplicative variant is used.|
| -snes_spin_action_rtol    | 1e-4                  | Relative tolerance used while evaluating action of SPIN operator on precond. residual.|


## Code compilation: 
1. UTOPIA:(Check detailed instructions on Utopia's bitbucket page for instalation) <br> 
bitbucket: https://bitbucket.org/zulianp/utopia/ <br> 
commit:1074078cf4c97346c354767793a8d9374d4aed21

2. MOOSE:  <br> 
git: https://github.com/idaholab/moose  <br> 
commit: 90123e7b6bd52f1bc36e68aac5d1fa95e76aeb91 

3. In file moose/framework/include/multiapps/MultiApp.h  <br> 
change function LocalRankConfig buildComm(bool batch_mode) to be virtual, i.e., virtual LocalRankConfig buildComm(bool batch_mode);


## Contributors: 
* Alena Kopanicakova (Brown, Providence and USI, Lugano) <alena.kopanicakova@brown.edu>, <alena.kopanicakova@usi.ch>
* Hardik Kothari (USI, Lugano) <hardik.kothari@usi.ch>